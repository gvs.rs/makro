/* -*- c-file-style:"stroustrup"; indent-tabs-mode: nil -*- */
/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#include "source/common.h"
#include "source/streams.h"

#include "source/utf8.hpp"

#include <cstdint>


constexpr bool ascii_identifikatora(int c) {
    return (c == '_') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') ||
        (c >= '0' && c <= '9');
}


constexpr bool znak_identifikatora(char32_t c) {
    return ascii_identifikatora(c) ||
        (c >= prvi_cyiril && c <= poslednyi_cyiril);
}


static int UcxitajIdentifikatorEx(KTok& ulaz, Identifikator_T& tIdentifikator)
{
    unsigned i = 0;
    int utf8_prvi = EOF;

    for (int c = ulaz.Cxitaj(); c != EOF; c = ulaz.Cxitaj()) {
        if (ascii_identifikatora(c)) {
            if (i < g_iMaxDuzinaIdent_C) {
                tIdentifikator[i++] = (char)c;
            }
            utf8_prvi = EOF;
        }
        else if (utf8_znak_prvi_od_dva(c)) {
            utf8_prvi = c;
        }
        else if ((EOF != utf8_prvi) && utf8_znak_drugi(c) && znak_identifikatora(utf8_od(utf8_prvi, c))) {
            if (i + 1 < g_iMaxDuzinaIdent_C) {
                tIdentifikator[i++] = (char)utf8_prvi;
                tIdentifikator[i++] = (char)c;
            }
            utf8_prvi = EOF;
        }
        else {
            if (EOF != utf8_prvi) {
                ulaz.Unread(utf8_prvi);
            }
            ulaz.Unread(c);
            break;
        }
    }

    tIdentifikator[i] = '\0';

    return i;
}


int UcitajIdentifikator(KTok& ulaz, Identifikator_T& tIdentifikator)
{
    int rzlt = UcxitajIdentifikatorEx(ulaz, tIdentifikator);
    return (rzlt > 0) ? 0 : -1;
}


DubokIdentifikator UcxitajDubokiIdentifikator(KTok& ulaz) {
    DubokIdentifikator rzlt;
    if (0 == UcitajIdentifikator(ulaz, rzlt.makro)) {
        if (int cxita = ulaz.Cxitaj(); '.' == cxita) {
            for (cxita = ulaz.Cxitaj(); cxita != EOF; cxita = ulaz.Cxitaj()) {
                if ('@' == cxita) {
                    ulaz.Unread(cxita);
                    break;
                }
                rzlt.param.append(1, (char)cxita);
            }
        }
        else {
            rzlt.param = rzlt.makro;
            rzlt.makro[0] = '\0';
            ulaz.Unread(cxita);
        }
    }
    return rzlt;;
}


void PreskociDoKrajaReda(KTok& ulaz)
{
    int iChar;

    do {
        iChar = ulaz.Cxitaj();
    } while (iChar != EOF && iChar != '\n');
}


void PreskociBeline(KTok& ulaz)
{
    int c;
    for (c = ' '; JeBelina(c); c = ulaz.Cxitaj()) {
        continue;
    }
    ulaz.Unread(c);
}
