/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#include "source/common.h"
#include "source/streams.h"

#include <stdio.h>
#include <memory>
#include <cstring>


#define CHECK(cond) if (cond) { } else Greska(#cond, __LINE__)


static void Greska(char const* sOpis, int iLine)
{
    printf("Neispunjen uslov: %s, linija: %d\n", sOpis, iLine);
    exit(1);
}


static void TestUcitajIdentifikator()
{
    printf("Test UcitajIdentifikator() ...\n");

    auto pStream = std::make_unique<KMemTok>(KTok::Rezxim::Pisxi);
    pStream->Pisxi("_IdEnT1+Ident_2*Ћиримџија!JakoDugacakIdentifikatorKojiImaViseOd32Karaktera");
    pStream->Iznova(KTok::Rezxim::Cxitaj);

    Identifikator_T tIdentifikator;
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "_IdEnT1"));
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) != 0);
    CHECK(pStream->Cxitaj() == '+');
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "Ident_2"));
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) != 0);
    CHECK(pStream->Cxitaj() == '*');
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "Ћиримџија"));
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) != 0);
    CHECK(pStream->Cxitaj() == '!');
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "JakoDugacakIdentifikatorKojiImaV"));
    CHECK(pStream->Cxitaj() == EOF);
}


static void TestPreskociDoKrajaReda()
{
    printf("Test PreskociDoKrajaReda() ...\n");

    auto pStream = std::make_unique<KMemTok>(KTok::Rezxim::Pisxi);
    pStream->Pisxi("abcd + efgh \nijkl\nmnop");
    pStream->Iznova(KTok::Rezxim::Cxitaj);

    Identifikator_T tIdentifikator;
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "abcd"));

    PreskociDoKrajaReda(*pStream);

    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "ijkl"));

    PreskociDoKrajaReda(*pStream);

    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "mnop"));
}


static void TestPreskociBeline()
{
    printf("Test PreskociBeline() ...\n");

    auto pStream = std::make_unique<KMemTok>(KTok::Rezxim::Pisxi);
    pStream->Pisxi("abcd + efgh \n ijkl\n");
    pStream->Iznova(KTok::Rezxim::Cxitaj);

    Identifikator_T tIdentifikator;
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "abcd"));

    PreskociBeline(*pStream);
    CHECK(pStream->Cxitaj() == '+');
    PreskociBeline(*pStream);

    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "efgh"));

    PreskociBeline(*pStream);

    CHECK(pStream->Cxitaj() == 'i');
    PreskociBeline(*pStream);
    CHECK(UcitajIdentifikator(*pStream, tIdentifikator) == 0);
    CHECK(!strcmp(tIdentifikator, "jkl"));
}


/*
 * Globalne funkcije
 */
int main()
{
    TestUcitajIdentifikator();
    TestPreskociDoKrajaReda();
    TestPreskociBeline();

    printf("COMMON test negativan\n");
    return 0;
}

