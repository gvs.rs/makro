/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#include "source/streams.h"

#include <cstdio>


int KTok::Cxitaj()
{
    if ((Rezxim::Pisxi == rezxim) || cxabar.empty()) {
        return EOF;
    }
    auto rzlt = cxabar.back();
    cxabar.pop_back();
    return rzlt;
}


int KTok::Pisxi(const char *sText)
{
    while (*sText != '\0') {
        if (Pisxi(*(sText++)))
            return -1;
    }
    return 0;
}


int KTok::Cxitaj(char *sText, int iLen)
{
    int procxitano = 0;
    while (procxitano < iLen) {
        if (auto c = Cxitaj(); c != EOF) {
            *sText++ = (char)c;
            ++procxitano;
	}
	else {
            break;
	}
    }
    *sText = '\0';

    return procxitano;
}


std::string KTok::CxitajDoKraja()
{
    std::string rzlt;
    for (int iChar = Cxitaj(); iChar != EOF; iChar = Cxitaj()) {
	rzlt += iChar;
    }
    return rzlt;;
}

int KTok::Unread(int iChar)
{
    if (iChar == '\n') {
        --red;
    }
    cxabar.push_back(iChar);
    return 0;
}


int KTok::Unread(char *sText, size_t iLen)
{
    for (size_t i = iLen; i > 0; --i) {
        if (Unread(sText[i-1]) != 0) {
            return -1;
        }
    }
    return 0;
}


KDatTok::KDatTok(std::string ime, Rezxim eMod) : KTok(eMod), imedat(std::move(ime))
{
    pFile = fopen(imedat.c_str(), (eMod == Rezxim::Cxitaj) ? "r" : "w");
}


KDatTok::~KDatTok()
{
    if (pFile != nullptr) {
        fclose(pFile);
    }
}


int KDatTok::Pisxi(char c)
{
    if ((rezxim != Rezxim::Pisxi) || (nullptr == pFile)) {
        return -1;
    }
    if (c == '\n') {
        ++red;
    }
    return (fputc(c, pFile) == EOF) ? -1 : 0;
}


int KDatTok::Cxitaj()
{
    if ((rezxim != Rezxim::Cxitaj) || (nullptr == pFile)) {
        return EOF;
    }

    int iChar = KTok::Cxitaj();
    if (iChar == EOF) {
        iChar = fgetc(pFile);
    }

    if ('\r' == iChar) {
        iChar = KTok::Cxitaj();
        if (EOF == iChar) {
            iChar = fgetc(pFile);
        }
        if (iChar != '\n') {
            if (iChar != EOF) {
                KTok::Unread(iChar);
            }
            iChar = '\n';
        }
    }

    if (iChar == '\n') {
        ++red;
    }

    return iChar;
}


int KDatTok::Iznova(Rezxim eMod)
{
    if (rezxim != eMod) {
        if (pFile != nullptr) {
            fclose(pFile);
        }

        KTok::Iznova(eMod);
        pFile = fopen(imedat.c_str(), (eMod == Rezxim::Cxitaj) ? "r" : "w");

        return (pFile == nullptr) ? -1 : 0;
    }
    else {
        if (pFile == nullptr) {
            return -1;
        }

        rewind(pFile);
        red = 1;

        return 0;
    }
}


KMemTok::KMemTok(Rezxim eMod) : KTok(eMod), poz(0)
{
}


int KMemTok::Pisxi(char c)
{
    if (rezxim != Rezxim::Pisxi) {
        return -1;
    }
    str.append(1, c);
    return 0;
}


int KMemTok::Cxitaj()
{
    if (rezxim != Rezxim::Cxitaj) {
        return EOF;
    }

    int iChar = KTok::Cxitaj();
    if (iChar != EOF) {
        return iChar;
    }

    if (poz < (int)str.size()) {
        return (unsigned char)(str[poz++]);
    }
    return EOF;
}


int KMemTok::Iznova(Rezxim eMod)
{
    KTok::Iznova(eMod);
    poz = 0;
    return 0;
}


int KMemTok::Pisxi(const char *sText)
{
    if (rezxim != Rezxim::Pisxi) {
        return -1;
    }
    str.append(sText);
    return 0;
}
