/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#include "source/rmacro.h"
#include "source/funkcije.hpp"
#include "source/streams.h"

#include "source/overloaded.hpp"
#include "source/nadyi.hpp"

#include <string_view>

#include <iostream>

#include <cstring>
#include <cassert>


static std::vector<SMakroDeklaracija> m_makroi;
static std::vector<std::shared_ptr<SRazvoj>> m_razvoji;

constexpr char sDeklaracije_C[] = "DEKLARACIJE";
constexpr char sRazvoj_C[] = "RAZVOJ";
constexpr std::string_view sMakro_C = "MAKRO";
constexpr char sMenja_C[] = "MENJA";


/** Pomocna funkcija koja proverava da li je zaglavlje makroa u razvoju
    isto kao u deklaraciji. Ovo je uvedeno cisto radi dodatne sigurnosti pri
    kodiranju, kao ispomoc programeru. Inace, samom programu to ne treba,
    on deklaracije vec ima.

    @pre Pozicija u @p ulaz je odmah nakon otvorene zagrade

    @param ulaz Tok iz koga treba ucitati parametre makroa iz zaglavlja
    @param pMakro Makro za koji se vrsi provera
    */
#
static void ProveraParamMakroa(KTok& ulaz, SMakroDeklaracija const& rMakro)
{
    auto it = rMakro.parametri.begin();
    for (;;) {
        Identifikator_T tImeParametra = "";
        if (UcitajIdentifikator(ulaz, tImeParametra)) {
            if ((it == rMakro.parametri.begin()) && (ulaz.Cxitaj() == ')')) {
                break;
            }
            throw XMakro(ulaz, "Ocekivano ime parametra '%s' u makrou '%s'", it->get()->ime.c_str(), rMakro.ime.c_str());
        }
        PreskociBeline(ulaz);

        if (it == rMakro.parametri.end()) {
            throw XMakro(ulaz, "Previse parametara za makro '%s'", rMakro.ime.c_str());
        }
        if (it->get()->ime != tImeParametra) {
            throw XMakro(ulaz, "Nedeklarisan parametar '%s' makroa '%s'", tImeParametra, rMakro.ime.c_str());
        }
        ++it;

        if (int iChar = ulaz.Cxitaj(); iChar == ')') {
            break;
        }
        else if (iChar != ',') {
            throw XMakro(ulaz, "Ocekivan zarez u zaglavlju makroa '%s'", rMakro.ime.c_str());
        }
        PreskociBeline(ulaz);
    }

    if (it != rMakro.parametri.end()) {
        throw XMakro(ulaz, "Premalo parametara za makro '%s', prvi koji fali je '%s'", rMakro.ime.c_str(), it->get()->ime.c_str());
    }
}

static std::string UcxitajDoZamene(KTok& ulaz, std::string const& ime)
{
    std::string rzlt;
    bool majmun = false;
    for (int cita = ulaz.Cxitaj(); cita != EOF; cita = ulaz.Cxitaj()) {
        if (!majmun) {
            if ('@' != cita) {
                rzlt.append(1, (char)cita);
            }
            else {
                majmun = true;
            }
        }
        else {
            switch (cita) {
            case ',':
                rzlt.append(1, '@');
                [[fallthrough]];
            case '\n':
                majmun = false;
                break;
            default:
                ulaz.Unread(cita);
                return rzlt;
            }
        }
    }
    throw XMakro(ulaz, "Neocekivani kraj datoteke u telu makroa '%s'", ime.c_str());
}


/** Ucitava definiciju (ili "telo") makroa. To je ono sto sledi posle zaglavlja,
    koje treba da bude isto kao u deklaraciji. Telo se zavrsava sa dva
    majmuna.

    @param ulaz Tok iz koga treba ucitati definiciju
    @param pMakro Makro u koji treba ucitati definiciju
    @param pTekuciRazvoj Razvoj datog makroa u koji treba ucitati definiciju
    @param kraj Ako je definicija kraja makroa
    @param privodno Prividno radno mesto, sluzxi samo da nam kazxe koje
        funkcije "mogu da prodyu" ucxitavanye
    */
static void UcitajDefinicijuMakroa(
    KTok& ulaz,
    SMakroDeklaracija& rMakro,
    std::shared_ptr<SRazvoj> pTekuciRazvoj,
    bool kraj,
    radno_mesto const& prividno
    )
{
    auto& def = kraj ? rMakro.kraj : rMakro.definicija;
    auto it = nadyi(def, &SMakroDefinicija::pRazvoj, pTekuciRazvoj);
    if ((it != def.end()) && !it->bMozeMenja) {
        throw XMakro(ulaz, "U definiciji ponovljeno ime makroa '%s'", rMakro.ime.c_str());
    }
    if (it == def.end()) {
        it = def.insert(it, SMakroDefinicija{});
    }
    it->zamena.clear();
    it->bMozeMenja = false;
    it->pRazvoj = pTekuciRazvoj;

    for (;;) {
        auto const text = UcxitajDoZamene(ulaz, rMakro.ime);
        if (!text.empty()) {
            it->zamena.emplace_back(text);
        }

        auto id = UcxitajDubokiIdentifikator(ulaz);
        if (id.param[0] == '\0') {
            // Mora da bude (drugi) majmun za kraj
            if (ulaz.Cxitaj() == '@')
                break;
            throw XMakro(ulaz, "Prazan parametar u telu makroa '%s'", rMakro.ime.c_str());
        }

        if (int iChar = ulaz.Cxitaj(); iChar == EOF) {
            throw XMakro(ulaz, "Neocekivan kraj datoteke u telu makroa '%s'", rMakro.ime.c_str());
        }
        else if (iChar != '@') {
            throw XMakro(ulaz, "Fali @ za kraj parametra u telu makroa '%s'", rMakro.ime.c_str());
        }

        if (id.makro[0] == '\0') {
            auto itpar = std::find_if(rMakro.parametri.begin(), rMakro.parametri.end(), [&](std::shared_ptr<SMakroParametar> const& par) {
                return par->ime == id.param;
            });
            if (itpar == rMakro.parametri.end()) {
                if (!prividno.mozxe(id.param)) {
                    throw XMakro(ulaz, "Koriscen nepoznat parametar '%s' u makrou '%s'", id.param.c_str(), rMakro.ime.c_str());
                }
                SMakroParametar makrpar;
                makrpar.ime = id.param;
                it->zamena.push_back(ParamZamena{std::make_shared<SMakroParametar>(makrpar)});
            }
            else {
                it->zamena.push_back(ParamZamena{*itpar});
            }
        }
        else {
            auto parmakdef = NadyiMakroDeklaraciju(id.makro);
            if (!parmakdef.has_value()) {
                if (!prividno.mozxe(id.makro)) {
                    throw XMakro(ulaz, "Koriscen nepoznat makro '%s' u makrou '%s'", id.makro, rMakro.ime.c_str());
                }
                SMakroParametar makrpar;
                makrpar.ime = id.param;
                it->zamena.push_back(BlokZamena{id.makro, std::make_shared<SMakroParametar>(makrpar)});
            }
            else {
                if (!ImaKraj(*parmakdef, pTekuciRazvoj)) {
                    throw XMakro(ulaz, "Koriscen makro '%s' u makrou '%s' nije blok-makro", id.makro, rMakro.ime.c_str());
                }
                auto itpar = std::find_if(parmakdef->parametri.begin(), parmakdef->parametri.end(), [&](std::shared_ptr<SMakroParametar> const& par) {
                    return par->ime == id.param;
                });
                if (itpar == parmakdef->parametri.end()) {
                    throw XMakro(ulaz, "Koriscen nepoznat parametar '%s' makroa '%s' u makrou '%s'", id.param.c_str(), id.makro, rMakro.ime.c_str());
                }
                else {
                    it->zamena.push_back(BlokZamena{id.makro, *itpar});
                }
            }
        }
    }
}


/** Ucitava deklaracije makroa (sa pocetka datoteke). Deklaracije se zavrsavaju
    sa dva majmuna.

    @param ulaz Tok iz koga treba ucitati deklaracije makroa
    @param makroi Gde treba dodati deklaracije
    */
static void UcitajDeklaracijeMakroa(KTok& ulaz, std::vector<SMakroDeklaracija> &makroi)
{
    /** Petlja: prolaz po jednom za svaki deklarisani makro */
    for (;;) {
        Identifikator_T tImeMakroa = "";

        if (0 == UcitajIdentifikator(ulaz, tImeMakroa)) {
            auto it = nadyi(makroi, &SMakroDeklaracija::ime, tImeMakroa);
            if (it != makroi.end()) {
                throw XMakro(ulaz, "U deklaraciji ponovljeno ime makroa '%s'", tImeMakroa);
            }

            SMakroDeklaracija macdec = { tImeMakroa, {}, {}, {}, false };
            it = makroi.insert(it, macdec);

            if (int iChar = ulaz.Cxitaj(); iChar != '(') {
                ulaz.Unread(iChar);
            }
            else {
                for (;;) {
                    /** Petlja: po jedan prolaz za svaki deklarisani parametar */
                    PreskociBeline(ulaz);

                    Identifikator_T tImeParametra = "";
                    if (UcitajIdentifikator(ulaz, tImeParametra)) {
                        if (it->parametri.empty()) {
                            if (iChar = ulaz.Cxitaj(); iChar == ')')
                                break;
                        }
                        throw XMakro(ulaz, "Sintaksna greska u deklaraciji - ocxekivano ime parametra ili zatvorena zagrada ako nema parametara");
                    }

                    auto itpar = std::find_if(it->parametri.begin(), it->parametri.end(), [&](auto& par) {
                        return par->ime == tImeParametra;
                    });
                    if (itpar != it->parametri.end()) {
                        throw XMakro(ulaz, "Ponovljeno ime parametra '%s'", tImeParametra);
                    }
                    SMakroParametar macpar;
                    macpar.ime = tImeParametra;
                    it->parametri.push_back(std::make_shared<SMakroParametar>(macpar));

                    PreskociBeline(ulaz);
                    if (iChar = ulaz.Cxitaj(); iChar == ')')
                        break;
                    else if (iChar != ',')
                        throw XMakro(ulaz, "Greska u deklaraciji - ocxekivan zarez za novi parametar ili zatvorena zagrada za kraj parametarak, dobijen %c", iChar);
                }
            }
        }
        else {
            if (int iChar = ulaz.Cxitaj(); iChar == '@') {
                break;
            }
            else switch (iChar) {
            case EOF:
                throw XMakro(ulaz, "Neocekivani kraj datoteke");
            case '/':
                if (ulaz.Cxitaj() != '/')
                    throw XMakro(ulaz, "Posle '/' u deklaraciji dozvolyena je samo josx jedna '/' za pocxetak komentara");
                PreskociDoKrajaReda(ulaz);
                break;
            default:
                throw XMakro(ulaz, "Sem makroa, u deklaraciji je dozvolyen samo komentar");
            }
        }
        PreskociBeline(ulaz);
    }

    if (ulaz.Cxitaj() != '@') {
        throw XMakro(ulaz, "Zavrsetak sekcije deklaracija mora da bude '@'");
    }
}


static std::optional<std::string> MakroKojiFali(
    std::shared_ptr<SRazvoj> pRazvoj,
    std::vector<SMakroDeklaracija> makroi
    )
{
    for (auto& mac: makroi) {
        auto it = nadyi(mac.definicija, &SMakroDefinicija::pRazvoj, pRazvoj);
        if (it == mac.definicija.end()) {
            return mac.ime;
        }
    }

    return {};
}


/** Vrsi kloniranje svih makroa za dati razvoj. Klonirani razvoj ima sve
    iste makroe, ali se isti mogu kasnije menjati. Posto su makroi u nekom
    "linearnom kontejneru", gde je za svaki clan "zakacen" linearni kontejner
    razvoja razvoja ("kako taj makro izgleda u tom razvoju"), ovo prakticno znaci
    dodavanje po jos jednog clana u svaki od tih kontejner razvoja, pri svakom
    makrou.

    @param pOriginal Pokazivac na originalni razvoj
    @param makroi Makroi koji se azxuriraju
    @param pKlonRazvoj Pokazivac na klonirani razvoj
    */
static void KlonirajRazvoj(
    std::shared_ptr<SRazvoj> pOriginal,
    std::vector<SMakroDeklaracija>& makroi,
    std::shared_ptr<SRazvoj> pKlonRazvoj
    )
{
    for (auto& mac: makroi) {
        auto it = nadyi(mac.definicija, &SMakroDefinicija::pRazvoj, pOriginal);
        assert(it != mac.definicija.end());

        struct SMakroDefinicija macdef;
        macdef.pRazvoj = pKlonRazvoj;
        macdef.zamena = it->zamena;
        macdef.bMozeMenja = true;
        mac.definicija.push_back(macdef);

        it = nadyi(mac.kraj, &SMakroDefinicija::pRazvoj, pOriginal);
        if (it != mac.kraj.end()) {
            macdef.pRazvoj = pKlonRazvoj;
            macdef.zamena = it->zamena;
            macdef.bMozeMenja = true;
            mac.kraj.push_back(macdef);
        }
    }
}


/** Ucitava deklaraciju razvoja iz datog toka. Razvoj moze da menja neki
    postojeci razvoj (lici na nasledjivanje iz OO programiranja). Ocigledno,
    moze da menja samo jedan razvoj (teorijski bi moglo da se smisli kako
    da ih menja i vise, ali ne vidim korist od toga, a mnogo bi zakomplikovalo
    i arhitekturu koda i sintaksu).

    @param ulaz Tok iz koga treba ucitati deklaraciju razvoja
    @param razvoji Vektor razvoja (referenca)
    @param tekuci Delyeni pokazivacx na trenutni razvoj (referenca)
    */
static void UcitajDeklRazvoja(
    KTok& ulaz,
    std::vector<std::shared_ptr<SRazvoj>>& razvoji,
    std::shared_ptr<SRazvoj>& tekuci
    )
{
    PreskociBeline(ulaz);

    Identifikator_T tImeRazvoja = "";
    if (UcitajIdentifikator(ulaz, tImeRazvoja))
        throw XMakro(ulaz, "Ime razvoja nepravilno '%s'", tImeRazvoja);

    for (auto razv: razvoji) {
        if (razv->ime == tImeRazvoja) {
            throw XMakro(ulaz, "Ponovljeno ime razvoja '%s'", tImeRazvoja);
        }
    }
    struct SRazvoj novi = { tImeRazvoja };
    razvoji.push_back(std::make_shared<SRazvoj>(novi));
    tekuci = razvoji.back();

    PreskociBeline(ulaz);

    Identifikator_T rec = "";
    UcitajIdentifikator(ulaz, rec);
    if (0 == strcmp(rec, sMenja_C)) {
        Identifikator_T imeRazvojOrig = "";

        PreskociBeline(ulaz);
        if (0 != UcitajIdentifikator(ulaz, imeRazvojOrig)) {
            throw XMakro(ulaz, "Greska u imenu izvornog razvoja za razvoj '%s'", tImeRazvoja);
        }

        auto it = std::find_if(razvoji.begin(), razvoji.end(), [&](auto r) {
            return r->ime == imeRazvojOrig;
        });
        if (it != razvoji.end()) {
            if (*it == tekuci) {
                throw XMakro(ulaz, "Ne moze razvoj da menja samog sebe!");
            }

            KlonirajRazvoj(*it, m_makroi, tekuci);
        }
        else {
            throw XMakro(ulaz, "Ne postoji originalni razvoj '%s'", imeRazvojOrig);
        }
    }
    else {
        ulaz.Unread(rec, strlen(rec));
    }
}


/*
 * Globalne funkcije
 */


void UcitajMakroe(KTok& ulaz)
{
    int iChar;
    Identifikator_T tIdent = "";

    PreskociBeline(ulaz);

    if (UcitajIdentifikator(ulaz, tIdent))
        throw XMakro(ulaz, "Nedostaje sekcija deklaracija - nema klyucxne recxi na pocxetku");
    if (strcmp(tIdent, sDeklaracije_C))
        throw XMakro(ulaz, "Nedostaje sekcija deklaracija - ocxekivan %s procxitano %s", sDeklaracije_C, tIdent);

    iChar = ulaz.Cxitaj();
    if (!JeBelina(iChar))
        throw XMakro(ulaz, "Nedostaje sekcija deklaracija - posle klyucxne recxi potreban je razmak");

    PreskociBeline(ulaz);

    std::shared_ptr<SRazvoj> pTekuciRazvoj;
    radno_mesto prividno;

    try {
    m_makroi.erase(m_makroi.begin(), m_makroi.end());
    UcitajDeklaracijeMakroa(ulaz, m_makroi);

    PreskociBeline(ulaz);

    m_razvoji.erase(m_razvoji.begin(), m_razvoji.end());


    for (;;) {
        if (0 == UcitajIdentifikator(ulaz, tIdent)) {
            if (0 == strcmp(tIdent, sRazvoj_C)) {
                if (!m_razvoji.empty()) {
                    if (auto falicxan = MakroKojiFali(pTekuciRazvoj, m_makroi)) {
                        throw XMakro(ulaz, "Prethodni razvoj '%s' nije kompletiran, fali '%s'", pTekuciRazvoj->ime.c_str(), falicxan->c_str());
                    }
                }

                UcitajDeklRazvoja(ulaz, m_razvoji, pTekuciRazvoj);
            }
            else if (sMakro_C == tIdent) {
                if (m_razvoji.empty())
                    throw XMakro(ulaz, "Makro nije unutar razvoja");

                PreskociBeline(ulaz);

                bool kraj = false;
                if (auto mozxda = ulaz.Cxitaj(); mozxda == '.') {
                    kraj = true;
                }
                else {
                    ulaz.Unread(mozxda);
                }
                Identifikator_T tImeMakroa;
                if (UcitajIdentifikator(ulaz, tImeMakroa))
                    throw XMakro(ulaz, "Nepravilno ime makroa '%s'", tImeMakroa);

                auto it = std::find_if(m_makroi.begin(), m_makroi.end(), [&](auto& mac) {
                    return mac.ime == tImeMakroa;
                });
                if (it == m_makroi.end()) {
                    throw XMakro(ulaz, "Makro '%s' nije deklarisan", tImeMakroa);
                }

                iChar = ulaz.Cxitaj();
                if (iChar != '(') {
                    ulaz.Unread(iChar);
                }
                else {
                    ProveraParamMakroa(ulaz, *it);
                }

                /** Ucitavanje definicije makroa */
                UcitajDefinicijuMakroa(ulaz, *it, pTekuciRazvoj, kraj, prividno);
            }
            else {
                throw XMakro(ulaz, "Nepoznata kljucna rec '%s'", tIdent);
            }

        }
        else {
            iChar = ulaz.Cxitaj();
            if (iChar == EOF)
                break;
            if (iChar != '/') {
                throw XMakro(ulaz, "%s%s Ocxekivan identifikator ili pocxetak komentara, dobijen znak: '%c'", (pTekuciRazvoj ? "Razvoj " : ""), (pTekuciRazvoj ? pTekuciRazvoj->ime.c_str() : ""), iChar);
            }
            iChar = ulaz.Cxitaj();
            if (iChar != '/') {
                throw XMakro(ulaz, "%s%s Posle prvog znaka komentara, ocxekivan drugi '/' a dobijen: '%c'", (pTekuciRazvoj ? "Razvoj " : ""), (pTekuciRazvoj ? pTekuciRazvoj->ime.c_str() : ""), iChar);
            }
            PreskociDoKrajaReda(ulaz);
        }
        PreskociBeline(ulaz);
    }
    }
    catch (XMakro& ex) {
        throw ex;
    }
    catch (std::exception& ex) {
        throw XMakro(ulaz, "Nepoznati izuzetak u obradi: %s", ex.what());
    }

    if (!m_razvoji.empty()) {
        if (auto falicxan = MakroKojiFali(pTekuciRazvoj, m_makroi)) {
            throw XMakro(ulaz, "Prethodni razvoj '%s' nije kompletiran, fali '%s'", pTekuciRazvoj->ime.c_str(), falicxan->c_str());
        }
     }
}


std::optional<SMakroDeklaracija> NadyiMakroDeklaraciju(std::string_view ime)
{
    auto it = nadyi(m_makroi, &SMakroDeklaracija::ime, std::string(ime));
    if (it != m_makroi.end()) {
        return *it;
    }
    else {
        return {};
    }
}


void PrintDefinicije(std::vector<SMakroDefinicija> const& def) {
    for (auto& macdef: def) {
        printf("        Razvoj %s:\n", macdef.pRazvoj->ime.c_str());

        for (auto zam: macdef.zamena) {
            std::visit(overloaded{
                [=](std::string const& s) {
                    printf("            \"%s\"\n", s.c_str());
                },
                [=](ParamZamena const& z) {
                    printf("            @%s@\n", z.param->ime.c_str());
                },
                [=](BlokZamena const& z) {
                    printf("            @%s.%s@\n", z.makro.c_str(), z.param->ime.c_str());
                }
            }, zam);
        }
    }
}

void MakroPrintout() {
    for (auto& mac : m_makroi) {
        printf("    %s(", mac.ime.c_str());
        bool prvi = true;
        for (auto& macpar: mac.parametri) {
            if (prvi) {
                printf("%s", macpar->ime.c_str());
                prvi = false;
            }
            else {
                printf(",%s", macpar->ime.c_str());
            }
        }
        printf(")(%zd)\nDefinicija\n", mac.parametri.size());
        PrintDefinicije(mac.definicija);
        printf("%s\n", !mac.kraj.empty() ? "Definicija kraja/zatvaranya" : "");
        PrintDefinicije(mac.kraj);
    }
}


std::vector<std::shared_ptr<SRazvoj>> const& UzmiRazvoje()
{
    return m_razvoji;
}
