/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#if !defined INC_RMACRO
#define INC_RMACRO

#include "source/common.h"
#include "source/streams.h"

#include <string>
#include <string_view>
#include <vector>
#include <memory>
#include <optional>
#include <variant>
#include <algorithm>


/** Deklaracija parametra makroa.  Trenutno je ovo samo ime, ali,
    ostavlyeno je kao mesto gde bi to moglo da se unapredi, recimo sa
    tipom ili drugim meta-podacima.
 */
struct SMakroParametar {
    std::string ime;
};

/** Zamena parametrom makroa u kome se zamena vrsxi */
struct ParamZamena {
    std::shared_ptr<SMakroParametar> param;
};

/** Zamena parametrom nekog od blok makroa u okviru koga se
    razvija ovaj "nasx" makro.
*/
struct BlokZamena {
    std::string makro;
    std::shared_ptr<SMakroParametar> param;
};


/// std::string je za "cxist tekst, bez zamene"
using MakroZamena = std::variant<std::string, ParamZamena, BlokZamena>;

struct SRazvoj {
    std::string ime;
};

struct SMakroDefinicija {
    std::shared_ptr<SRazvoj> pRazvoj;
    std::vector<MakroZamena> zamena;

    /// Definicija mozxe da se menya ako je nastala kloniranyem
    /// iz postojecyeg razvoja.
    bool bMozeMenja = false;
};


struct SMakroDeklaracija {
    std::string ime;
    std::vector<std::shared_ptr<SMakroParametar>> parametri;

    /// Definicije, po jedna za svaki razvoj
    std::vector<SMakroDefinicija> definicija;

    /// Definicije kraja, po jedna za svaki razvoj u kome je definisan
    /// kraj.
    std::vector<SMakroDefinicija> kraj;

    /// Razvijanye je u toku za ovaj makro. Pre svega sluzxi za
    /// sprecxavanye rekurzije, da neki makro ne proba da razvije "sam
    /// sebe". Ovde smo alternativno mogli da se ponasxamo kao C++
    /// preprocesor i u tom slucxaju razvijemo ime makroa, ali, to
    /// nije u duhu MAKRO razvoja.
    ///
    /// TODO izmestiti ovo, posxto da li je razvijanye ovog makroa u
    /// toku nema basx veze sa deklaracijom istog.
    bool radan;
};

inline bool ImaKraj(SMakroDeklaracija const& makro, std::shared_ptr<SRazvoj> p){
    auto it = std::find_if(makro.kraj.begin(), makro.kraj.end(), [=](SMakroDefinicija const &mdef) {
	return mdef.pRazvoj == p;
    });
    return it != makro.kraj.end();
}


class XMakro : public std::exception {
    public:
    XMakro(KTok& dat, char const* fmt, char const* p1 = nullptr, char const* p2 = nullptr, char const* p3 = nullptr) : tok(dat) {
        snprintf(s, sizeof s, fmt, p1, p2, p3);
    }
    XMakro(KTok& dat, char const* fmt, char const* p, char const* pp, char c) : tok(dat) {
        snprintf(s, sizeof s, fmt, p, pp, c);
    }
    XMakro(KTok& dat, char const* fmt, char const* p, char c) : tok(dat) {
        snprintf(s, sizeof s, fmt, p, c);
    }
    XMakro(KTok& dat, char const* fmt, char c) : tok(dat) {
        snprintf(s, sizeof s, fmt, c);
    }

    const char* what() const noexcept override { return s; }
    KTok& Tok() const { return tok; }

private:
    char s[256];
    KTok& tok;
};

void UcitajMakroe(KTok& ulaz);

std::optional<SMakroDeklaracija> NadyiMakroDeklaraciju(std::string_view ime);

std::vector<std::shared_ptr<SRazvoj>> const& UzmiRazvoje();

void MakroPrintout();


#endif // !defined INC_RMACRO
