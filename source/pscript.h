/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#if !defined INC_PSCRIPT
#define INC_PSCRIPT

#include <string>
#include <memory>


class KDatTok;

class XSkript {
public:
    XSkript(const char *sErr, KDatTok& ulaz, char const* par=nullptr);
    XSkript(const char *sErr, KDatTok& ulaz, int i, char const* par);
    XSkript(const char *sErr, KDatTok& ulaz, char const* p1, char const* p2, char const* p3);
    XSkript(const char *sErr, KDatTok& ulaz, char const* p1, char const* p2, char const* p3, char const* p4);

    std::string const& UzmiErr() const {
        return sErr;
    }

    std::string const& ImeDat() const {
        return imedat;
    }

    int Red() const {
        return red;
    }

private:
    const int red;
    std::string sErr;
    std::string imedat;
};

struct SRazvoj;
class KTok;

void ObradaSkripta(KDatTok& ulaz, KTok *pIzlaz, std::shared_ptr<SRazvoj> pRazvoj);

#endif // !defined INC_PSCRIPT
