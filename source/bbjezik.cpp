/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2024-2024
 * @license GPLv3
 ***********************************************/

#include "bbjezik.hpp"

#include <vector>

#include <cctype>


#include <iostream>


std::vector<std::string_view> podeli(std::string_view str, std::string_view delimiter)
{
    std::vector<std::string_view> rzlt;

    for (auto pos = str.find(delimiter); pos != str.npos; pos = str.find(delimiter)) {
        rzlt.emplace_back(str.substr(0, pos));
        str.remove_prefix(pos + delimiter.length());
    }

    if (!str.empty()) {
        rzlt.emplace_back(str);
    }
    return rzlt;
}


void preskocxi_beline(std::string_view& str)
{
    while (!str.empty() && std::isspace(static_cast<unsigned char>(str.front()))) {
        str.remove_prefix(1);
    }
}


void odbaci_beline(std::string_view& str)
{
    preskocxi_beline(str);
    while (!str.empty() && std::isspace(static_cast<unsigned char>(str.back()))) {
        str.remove_suffix(1);
    }
}

std::string bbj_izvrsxi(std::string_view                                    program,
                        radno_mesto&                                        rame,
                        std::unordered_map<std::string, std::string> const& param)
{
    auto                                                   parcxad = podeli(program, ";");
    std::string                                            rzlt;
    std::unordered_map<std::string_view, std::string_view> prog_param;
    for (auto& parcxe : parcxad) {
        if (parcxe.empty()) {
            rzlt += ';';
        }
        else {
            auto pos = parcxe.find('.');
            if (pos != parcxe.npos) {
                auto sim = parcxe.substr(0, pos);
                preskocxi_beline(sim);
                auto prm = parcxe.substr(pos + 1);
                if (sim == "prm") {
                    pos = prm.find('=');
                    if (pos != prm.npos) {
                        auto ime = prm.substr(0, pos);
                        odbaci_beline(ime);
                        prog_param[ime] = prm.substr(pos + 1);
                    }
                }
                else {
                    std::string param_za_poziv;
                    if (param.count(std::string(prm)) == 1) {
                        param_za_poziv = param.find(std::string(prm))->second;
                    }
                    else if (prog_param.count(prm) == 1) {
                        param_za_poziv = prog_param[prm];
                    }
                    else {
                        param_za_poziv = prm;
                    }
                    rzlt += rame.zovi(sim, param_za_poziv);
                }
            }
            else {
                rzlt += parcxe;
            }
        }
    }

    return rzlt;
}
