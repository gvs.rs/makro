/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#if !defined(INC_MEMPTR_TRAITS)
#define INC_MEMPTR_TRAITS


template <class T> struct memptr_traits;
template <class T, class M> struct memptr_traits<M T::*> {
    using klass = T;
    using typ = M;
};


#endif // !defined(INC_MEMPTR_TRAITS)
