/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#include "source/template.h"
#include "source/pscript.h"
#include "source/rmacro.h"
#include "source/streams.h"

#include <iostream>


static bool isti(KDatTok& f, KDatTok& ocx)
{
    for (int c = f.Cxitaj(); c != EOF; c = f.Cxitaj()) {
        if (c != ocx.Cxitaj()) {
            return false;    
        }
    }
    return ocx.Cxitaj() == EOF;
}


inline void gresxka(char const* fmt, char const* par) {
    char s[256];
    snprintf(s, sizeof s, fmt, par);
    throw std::invalid_argument(s);
}


inline void gresxka(char const* fmt, char const* par, char const* par2) {
    char s[256];
    snprintf(s, sizeof s, fmt, par, par2);
    throw std::invalid_argument(s);
}


static void proveri(std::string const& skript_ime, std::string const& sxablon_ime, std::string const& ocxekivan_ime)
{
    printf("Proveravam da li se skriptom %s kroz sxablon %s dobija %s\n", skript_ime.c_str(), sxablon_ime.c_str(), ocxekivan_ime.c_str());

    KDatTok sxablon(sxablon_ime.c_str(), KTok::Rezxim::Cxitaj);
    if (!sxablon.JeValidan()) {
        gresxka("Greska u pozivu testa: ne moze da se otvori datoteka %s\n", sxablon_ime.c_str());
    }
    KDatTok skript(skript_ime.c_str(), KTok::Rezxim::Cxitaj);
    if (!skript.JeValidan()) {
        gresxka("Greska u pozivu testa: ne moze da se otvori datoteka %s\n", skript_ime.c_str());
    }
    constexpr char izlaz_ime[] = "priv_izlaz";
    auto izlaz = std::make_unique<KDatTok>(izlaz_ime, KTok::Rezxim::Pisxi);
    if (!izlaz->JeValidan()) {
        gresxka("Greska u pozivu testa: ne moze da se otvori datoteka %s\n", izlaz_ime);
    }

    PrevediTemplate(sxablon, izlaz.get(), skript);

    KDatTok ocxekivan(ocxekivan_ime.c_str(), KTok::Rezxim::Cxitaj);
    if (!ocxekivan.JeValidan()) {
        gresxka("Greska u pozivu testa: ne moze da se otvori datoteka %s\n", ocxekivan_ime.c_str());
    }
    izlaz->Iznova(KTok::Rezxim::Cxitaj);
    if (!isti(*izlaz, ocxekivan)) {
        gresxka("Dobijena C datoteka %s se razlikuje od ocxekivane %s\n", izlaz_ime, ocxekivan_ime.c_str());
    }
    izlaz.reset();

    remove(izlaz_ime);
}


static void proveridir(std::string const& koren) {
    constexpr auto c_sxablon = "enumc.tem";
    constexpr auto h_sxablon = "enumh.tem";
    constexpr auto skript = "enums.scr";
    constexpr auto ocxekivan_c = "enum.c.exp";
    constexpr auto ocxekivan_h = "enum.h.exp";
    proveri(koren + "/" + skript, koren + "/" + c_sxablon, koren + "/" + ocxekivan_c);
    proveri(koren + "/" + skript, koren + "/" + h_sxablon, koren + "/" + ocxekivan_h);
}


int main(int argc, char **argv)
{
    if (argc != 2) {
        printf("Greska u pozivu testa: ocekivan 1 parametar\n");
        return 1;
    }
    std::string koren = argv[1];

    try {
	proveridir(koren + "/test/enum");
	proveridir(koren + "/test/enumblok");
    }
    catch (XSkript& x) {
	std::cout << "Gresxka: " << x.UzmiErr() << ", datoteka: " << x.ImeDat() << ", red: " << x.Red() << std::endl;
	return -1;
    }
    catch (std::exception& x) {
        printf("Neocxekivana Gresxka: %s\n", x.what());
	return -1;
    }

    return 0;
}
