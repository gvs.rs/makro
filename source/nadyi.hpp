/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#if !defined(INC_NADYI)
#define INC_NADYI

#include "source/memptr_traits.h"

#include <algorithm>

/**
 * Slicxno std::find(), samo sxto se trazxi po vrednosti jednog cxlana
 * sloga. Prilicxno skracyuje kolicxinu koda da se iskazxe prosta stvar.
 */
template <class I, class MP>
I nadyi(I b, I e, MP mp, typename memptr_traits<MP>::typ v) {
    return std::find_if(b, e, [=](auto& x) {
        return v == x.*mp;
    });
}

/**
 * Varijanta trazxenya po vrednosti cxlana nad "kontejnerom", radi
 * manyeg kucanya.
 */
template <class C, class MP>
auto nadyi(C& c, MP mp, typename memptr_traits<MP>::typ v) -> decltype(c.begin()) {
    return std::find_if(c.begin(), c.end(), [&](auto& x) {
        return v == x.*mp;
    });
}

/**
 * Varijanta trazxenya po vrednosti cxlana nad "kontejnerom" _unazad_, radi
 * manyeg kucanya.
 */
template <class C, class MP>
auto rnadyi(C& c, MP mp, typename memptr_traits<MP>::typ v) -> decltype(c.rbegin()) {
    return std::find_if(c.rbegin(),c.rend(),[=](auto& x) {
        return v == x.*mp;
    });
}


#endif // !defined(INC_NADYI)
