/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2024-2024
 * @license GPLv3
 ***********************************************/

#if !defined INC_FUNKCIJE
#define INC_FUNKCIJE

#include <string_view>
#include <string>
#include <functional>
#include <unordered_map>


struct parfje;
using funkcija = std::function<std::string(parfje&)>;

struct parfje {
    std::string_view arg;
    funkcija tek;
};

/// Marija Maga Magazinovicy, prva bibliotekarka Narodne Biblioteke
/// Da ne bude bezlicxno "bibliotekarka"
struct magazinovicy;

class radno_mesto {
public:
    radno_mesto();

    std::string zovi(std::string_view fju) {
	return zovi(fju, "");
    }
    std::string zovi(std::string_view fju, std::string_view arg);

    // Da li funkcija @p fja postoji trenutno (sistemska ili korisnicxka)
    bool ima(std::string_view fja) const;

    // Da li funkcija @p fja trenutno psstoji ili mozxe da se uvede kao korisnicxka
    bool mozxe(std::string_view fja) const;

private:
    friend struct magazinovicy;
    std::unordered_map<std::string, funkcija> biblioteka;
    parfje tekucyi;
};


#endif // !defined INC_FUNKCIJE
