/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#include "source/rmacro.h"
#include "source/pscript.h"
#include "source/funkcije.hpp"
#include "source/streams.h"

#include "overloaded.hpp"
#include "nadyi.hpp"

#include <cassert>
#include <cctype>
#include <algorithm>
#include <numeric>
#include <stdexcept>


using argtokovi = std::vector<std::shared_ptr<KMemTok>>;

struct otvaranye {
    std::string ime;
    argtokovi arg;
};

enum class makro {
    obicxan,
    kraj
};


static void ObradaSkripta(KDatTok& ulaz, KTok *pIzlaz, std::shared_ptr<SRazvoj> pRazvoj, size_t iParamsLeft, std::vector<otvaranye>& otvaranya, radno_mesto& radno);


inline makro odslova(int slovo) {
    switch (slovo) {
    case '@':
        return makro::obicxan;
    case '.':
        return makro::kraj;
    default:
        throw std::invalid_argument("Nepoznato drugo slovo u pozivu makroa");
    }
}


inline char uchar(makro m) { return (m == makro::kraj) ? '.' : '@'; }


inline std::string zovi(KDatTok& ulaz, std::shared_ptr<SRazvoj> pRazvoj, SMakroDeklaracija const& dekla, radno_mesto& rame, std::string const& fju, std::string const& arg) {
    if (!rame.ima(fju)) {
        throw XSkript("Razvoj %s makro %s Nepostojecya funkcija %s", ulaz, pRazvoj->ime.c_str(), dekla.ime.c_str(), fju.c_str());
    }
    try {
        return rame.zovi(fju, arg);
    }
    catch (std::exception& ex) {
        throw XSkript("Razvoj %s makro %s Gresxka u izvrsxavanyu funkcije %s: %s", ulaz, pRazvoj->ime.c_str(), dekla.ime.c_str(), fju.c_str(), ex.what());
    }
}


inline void Razvi(makro vrsta, SMakroDeklaracija const& dekla, argtokovi const& argumenti, KDatTok& ulaz, std::shared_ptr<SRazvoj> pRazvoj, KTok *pIzlaz, std::vector<otvaranye>& otvaranya, radno_mesto& radno) {
    auto& def = (makro::obicxan == vrsta) ? dekla.definicija : dekla.kraj;
    auto itdef = nadyi(def, &SMakroDefinicija::pRazvoj, pRazvoj);
    assert((makro::kraj == vrsta) || (itdef != def.end()));
    if (itdef == def.end()) {
        return;
    }
    for (auto zam: itdef->zamena) {
        std::visit(overloaded{
                [&](std::string const& s) {
                    pIzlaz->Pisxi(s.c_str());
                },
                [&](ParamZamena const& z) {
                    auto itpar = std::find(dekla.parametri.begin(), dekla.parametri.end(), z.param);
                    if (itpar == dekla.parametri.end()) {
                        assert(radno.mozxe(z.param->ime));
                        pIzlaz->Pisxi(zovi(ulaz, pRazvoj, dekla, radno, z.param->ime, "").c_str());
                    }
                    else {
                        auto i = std::distance(dekla.parametri.begin(), itpar);
                        auto arg = std::next(argumenti.begin(), i)->get();
                        pIzlaz->Pisxi(arg->CxitajSve().c_str());
                    }
                },
                [&](BlokZamena const& z) {
                    auto itotv = rnadyi(otvaranya, &otvaranye::ime, z.makro);
                    if (itotv != otvaranya.rend()) {
                        auto blok = NadyiMakroDeklaraciju(z.makro);
                        assert(blok.has_value());
                        auto itpar = std::find(blok->parametri.begin(), blok->parametri.end(), z.param);
                        assert(itpar != blok->parametri.end());
                        auto i = std::distance(blok->parametri.begin(), itpar);
                        auto arg = std::next(itotv->arg.begin(), i)->get();
                        pIzlaz->Pisxi(arg->CxitajSve().c_str());
                    }
                    else if (radno.ima(z.makro)) {
                        auto itpar = std::find_if(dekla.parametri.begin(), dekla.parametri.end(), [=](auto param) {
                            return param->ime == z.param->ime;
                            });
                        if (itpar == dekla.parametri.end()) {
                            pIzlaz->Pisxi(zovi(ulaz, pRazvoj, dekla, radno, z.makro, z.param->ime).c_str());
                        }
                        else {
                            auto i = std::distance(dekla.parametri.begin(), itpar);
                            auto arg = std::next(argumenti.begin(), i)->get();
                            pIzlaz->Pisxi(zovi(ulaz, pRazvoj, dekla, radno, z.makro, arg->CxitajSve()).c_str());
                        }
                    }
                    else throw XSkript("Razvoj '%s', Blok makro '%s', cxiji argument '%s' je korisxcyen, nije ni otvoren", ulaz, pRazvoj->ime.c_str(), z.makro.c_str(), z.param->ime.c_str());
                }
            }, zam);
    }
}


static void ObradaMakroa(KDatTok& ulaz, KTok *pIzlaz, std::shared_ptr<SRazvoj> pRazvoj, makro vrsta, std::vector<otvaranye>& otvaranya, radno_mesto& radno)
{
    Identifikator_T tImeMakroa = "";
    if (UcitajIdentifikator(ulaz, tImeMakroa))
        throw XSkript("Lose zadato ime makroa '%s'", ulaz, tImeMakroa);

    auto it = NadyiMakroDeklaraciju(tImeMakroa);
    if (!it.has_value()) {
        throw XSkript("Iskoriscen nepostojeci makro %s", ulaz, tImeMakroa);
    }

    if (it->radan) {
        throw XSkript("Beskonacna rekurzija", ulaz);
    }
    it->radan = true;

    argtokovi argumenti;
    for (auto& par : it->parametri) {
        argumenti.push_back(std::make_unique<KMemTok>(KTok::Rezxim::Pisxi));
    }

    if (auto iChar = ulaz.Cxitaj(); iChar == '(') {
        if (makro::kraj == vrsta) {
            throw XSkript("Kraj/zatvaranye blok-makroa ne mozxe da ima parametre", ulaz);
        }
        auto size = argumenti.size();
        for (auto& arg : argumenti) {
          ObradaSkripta(ulaz, arg.get(), pRazvoj, size--, otvaranya, radno);
        }
        if (ImaKraj(*it, pRazvoj)) {
            otvaranya.push_back({tImeMakroa, argumenti});
        }
    }
    else {
        if (makro::obicxan == vrsta) {
            if (!argumenti.empty()) {
                throw XSkript("Makro s parametrima '%s' koriscen kao da ih nema", ulaz, tImeMakroa);
            }
            if (ImaKraj(*it, pRazvoj)) {
                otvaranya.push_back({tImeMakroa, argumenti});
            }
        }
        else {
            auto itotv = rnadyi(otvaranya, &otvaranye::ime, tImeMakroa);
            if (itotv == otvaranya.rend()) {
                throw XSkript("Zatvaranye makroa '%s' koji nije ni otvoren", ulaz, tImeMakroa);
            }
            argumenti = itotv->arg;
            otvaranya.erase(std::next(itotv).base());
        }
        ulaz.Unread(iChar);
    }

    Razvi(vrsta, *it, argumenti, ulaz, pRazvoj, pIzlaz, otvaranya, radno);

    it->radan = false;
}


static void ObradaSkripta(KDatTok& ulaz, KTok *pIzlaz, std::shared_ptr<SRazvoj> pRazvoj, size_t iParamsLeft, std::vector<otvaranye>& otvaranya, radno_mesto& radno)
{
    bool blZagrada = false;

    for (;;) {
        const int iChar = ulaz.Cxitaj();
        const int iEscape = (iChar == '@') ? ulaz.Cxitaj() : 0;

        if (iChar == '@' && iEscape == '/') {
            PreskociDoKrajaReda(ulaz);
            continue;
        }
        if (iChar == EOF)
            throw XSkript("Neocekivani kraj fajla", ulaz);
        if (blZagrada) {
            if (iChar != '@')
                pIzlaz->Pisxi((char)iChar);
            else switch (iEscape) {
                case '[':
                    throw XSkript("Ugnjezdene zagrade '@['", ulaz);
                case ']':
                    blZagrada = false;
                    break;
                case '@':
                case '.':
                    ObradaMakroa(ulaz, pIzlaz, pRazvoj, odslova(iEscape), otvaranya, radno);
                    break;
                default:
                    throw XSkript("Sintaksna greska u skriptu", ulaz);
                }
        }
        else {
            switch (iChar) {
            case  ',':
                if (iParamsLeft > 1)
                    return;
                throw XSkript("Neslaganje broja parametara sa deklarisanim", ulaz);
            case ')':
                if (iParamsLeft == 1)
                    return;
                throw XSkript("Neslaganje broja parametara sa deklarisanim", ulaz);
            case '@':
                switch (iEscape) {
                case '[':
                    blZagrada = true;
                    break;
                case ']':
                    throw XSkript("Nema otvorene zagrade '@['", ulaz);
                case '@':
                case '.':
                    ObradaMakroa(ulaz, pIzlaz, pRazvoj, odslova(iEscape), otvaranya, radno);
                    break;
                default:
                    throw XSkript("Sintaksna greska u skriptu", ulaz);
                }
                break;
            default:
                pIzlaz->Pisxi((char)iChar);
                break;
            }
        }
    }
}


static void UkljuciSkript(KDatTok& ulaz, KTok *pIzlaz, std::shared_ptr<SRazvoj> pRazvoj)
{
    std::string imedat{ulaz.ImeDat()};

    try {
        if (auto pos = imedat.find_last_of("/\\"); pos != imedat.npos) {
            imedat.resize(pos+1);
        }
        imedat += ulaz.CxitajRedDo('\n');
        PreskociBeline(ulaz);
    }
    catch (std::exception& ex) {
        throw XSkript("Neuspelo prepoznavanye imena \'include\' datoteke: %s", ulaz, ex.what());
    }

    KDatTok file(imedat, KTok::Rezxim::Cxitaj);
    if (!file.JeValidan()) {
        throw XSkript("Neuspelo otvaranje \'include\' fajla", ulaz);
    }

    ObradaSkripta(file, pIzlaz, pRazvoj);
}


/*
 * Globalne funkcije
 */
void ObradaSkripta(KDatTok& ulaz, KTok *pIzlaz, std::shared_ptr<SRazvoj> pRazvoj)
{
    std::vector<otvaranye> otvaranya;
    radno_mesto radno;
    try {
        for (;;) {
            PreskociBeline(ulaz);
            int iChar = ulaz.Cxitaj();
            if (iChar == EOF)
                break;

            if (iChar != '@')
                throw XSkript("U skriptu smeju biti samo makroi", ulaz);

            switch (iChar = ulaz.Cxitaj()) {
            case '/':
                PreskociDoKrajaReda(ulaz);
                break;
            case '\"':
                UkljuciSkript(ulaz, pIzlaz, pRazvoj);
                break;;
            case '@':
            case '.':
                ObradaMakroa(ulaz, pIzlaz, pRazvoj, odslova(iChar), otvaranya, radno);
            break;
            default:
                throw XSkript("Sintaksna greska u skriptu", ulaz);
            }
        }
        if (!otvaranya.empty()) {
            std::string fale = "Sledecya otvaranya nisu zatvorena:\n";
            for (auto const& o : otvaranya) {
                fale += o.ime + '(';
                if (!o.arg.empty()) {
                    fale += std::accumulate(std::next(o.arg.begin()), o.arg.end(), o.arg.front()->CxitajSve(), [=](std::string const&a, auto& v) {
                        return a + ',' + v->CxitajSve();
                    });
                }
                fale += ')';
            }

            throw XSkript(fale.c_str(), ulaz);
        }
    }
    catch (XSkript& ex) {
        throw ex;
    }
    catch (XMakro& ex) {
        throw XSkript("makroteka(%d): %s", ulaz, ex.Tok().Red(), ex.what());
    }
    catch (std::exception& ex) {
        throw XSkript("Nepoznati izuzetak: %s", ulaz, ex.what());
    }
}


XSkript::XSkript(const char *fmt, KDatTok& ulaz, char const* par)
    : red(ulaz.Red())
    , imedat(ulaz.ImeDat())
{
    if (par) {
        char s[200];
        snprintf(s, sizeof s, fmt, par);
        sErr = s;
    }
    else {
        sErr = fmt;
    }
}


XSkript::XSkript(const char *fmt, KDatTok& ulaz, int i, char const* par)
    : red(ulaz.Red())
    , imedat(ulaz.ImeDat())
{
    char s[200];
    snprintf(s, sizeof s, fmt, i, par);
    sErr = s;
}


XSkript::XSkript(const char *fmt, KDatTok& ulaz, char const* p1, char const* p2, char const* p3)
    : red(ulaz.Red())
    , imedat(ulaz.ImeDat())
{
    char s[256];
    snprintf(s, sizeof s, fmt, p1, p2, p3);
    sErr = s;
}


XSkript::XSkript(const char *fmt, KDatTok& ulaz, char const* p1, char const* p2, char const* p3, char const* p4)
    : red(ulaz.Red())
    , imedat(ulaz.ImeDat())
{
    char s[256];
    snprintf(s, sizeof s, fmt, p1, p2, p3, p4);
    sErr = s;
}
