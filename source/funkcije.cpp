/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2024-2024
 * @license GPLv3
 ***********************************************/

#include "source/funkcije.hpp"

#include <regex>
#include <unordered_set>


class f_iota {
    unsigned x = 0;
public:
    std::string operator()(parfje& a) {
        if (a.arg == "nov") {
            a.tek = f_iota();
            return "";
        }
        else {
            return std::to_string(x++);
        }
    }
};

class f_bool {
    const bool rez;
public:
    f_bool(bool b) : rez(b) {}
    std::string operator()(parfje&) {
        return rez ? "⊤" : "";
    }
};

class f_jedini {
  std::unordered_set<std::string> skup;
public:
  std::string operator()(parfje& a) {
    if (a.arg.empty()) {
      a.tek = f_jedini();
      return "";
    }
    auto [i, uspeh] = skup.insert(std::string(a.arg));
    a.tek = f_bool(uspeh);
    return "";
  }
};

const std::unordered_set<std::string_view> pusti_me = {
    "ne", "не", "no", "false", "нет",
    "", "0",
    "⊥" // mozxda i "\\bot", LaTeX za ovaj znak
};

inline bool netacxno(std::string_view s) {
    return pusti_me.count(s) == 1;
};

class f_negiraj {
public:
    std::string operator()(parfje& a) {
        auto jel = !netacxno(a.tek(a));
	a.tek = f_bool(!jel);
	return "";
    }
};

class f_regex_zamena {
    std::regex regex;
    std::string zamena;

public:
    f_regex_zamena(std::regex r, std::string_view z) : regex(r), zamena(z) {}

    std::string operator()(parfje& a) {
        return std::regex_replace(std::string(a.arg), regex, zamena, std::regex_constants::format_no_copy | std::regex_constants::format_sed);
    }

};

class f_regex_trazxi {
    std::regex regex;

public:
    f_regex_trazxi(std::regex r) : regex(r) {}

    std::string operator()(parfje& a) {
        std::smatch m;
        std::string s(a.arg);
        if (std::regex_search(s, m, regex)) {
            return m[0];
        }
        return "";
    }

};

class f_regex {
public:
    std::string operator()(parfje& a) {
        if (a.arg.size() <= 3) {
            throw std::invalid_argument("regex: izraz previsxe kratak");
        }
        if (a.arg[0] != '/') {
            throw std::invalid_argument("regex: izraz mora da pocxne sa kosom crtom ('/')");
        }
        auto pos = a.arg.find('/', 1);
        if (pos == a.arg.npos) {
            throw std::invalid_argument("regex: izraz mora da se zavrsi sa kosom crtom ('/')");
        }
        auto program = a.arg.substr(1, pos-1);

        if (auto kraj = a.arg.find('/', pos+1); kraj != a.arg.npos) {
	    a.tek = f_regex_zamena(std::regex(program.begin(), program.end()), a.arg.substr(pos + 1, kraj - pos - 1));
        }
	else {
            a.tek = f_regex_trazxi(std::regex(program.begin(), program.end()));
	}
        return "";
    }
};

struct f_nisx {
    std::string operator()(parfje& a) {
        throw std::invalid_argument("Katastrofa! Ovo nije trebalo da bude pozvano");
    }
};

struct magazinovicy {
    static void uvedi(radno_mesto& rame, std::string_view ime, funkcija& fja) {
        if (ime.find('_') != 0) {
            throw std::invalid_argument("Ime korisnicxke funkcije mora da pocxne sa podvlakom/donyom crtomm `_`");
        }
        rame.biblioteka[std::string(ime)] = fja;
    }
    static std::string zovi(radno_mesto& rame, std::string_view fju, std::string_view arg) {
        if (!rame.ima(fju)) {
            throw std::invalid_argument(std::string("Ne postoji funkcija ") + std::string(fju));
        }
        return rame.zovi(fju, arg);
    }
};

class f_uvedi {
    radno_mesto& rame;
public:
    f_uvedi(radno_mesto& r) : rame(r) {}
    std::string operator()(parfje& a) {
        magazinovicy::uvedi(rame, a.arg, a.tek);
        return "";
    }
};

class f_uvedi_novu {
    radno_mesto& rame;
public:
    f_uvedi_novu(radno_mesto& r) : rame(r) {}
    std::string operator()(parfje& a) {
        if (rame.ima(a.arg)) {
            throw std::invalid_argument("Korisnicxko ime vecy postoji");
        }
        magazinovicy::uvedi(rame, a.arg, a.tek);
        return "";
    }
};

struct f_izvrsxi {
    std::string operator()(parfje& a) {
        return a.tek(a);
    }
};

class f_zovi {
    radno_mesto& rame;
public:
    f_zovi(radno_mesto& r) : rame(r) {}

    std::string operator()(parfje& a) {
        return magazinovicy::zovi(rame, a.arg, "");
    }
};

struct f_odbaci {
    std::string operator()(parfje& a) {
        a.tek = f_nisx();
        return "";
    }
};

struct f_zavrsxi {
    std::string operator()(parfje& a) {
        auto r = a.tek(a);
        a.tek = f_nisx();
        return r;
    }
};

class f_ako {
public:
    std::string operator()(parfje& a) {
        a.tek = f_bool(!netacxno(a.arg));
        return "";
    }
};

class f_fako {
    radno_mesto& rame;
public:
    f_fako(radno_mesto& rm) : rame(rm) {}
    std::string operator()(parfje& a) {
        a.tek = f_bool(!netacxno(magazinovicy::zovi(rame, a.arg, "")));
        return "";
    }
};

struct f_onda {
    std::string operator()(parfje& a) {
        auto jel = !a.tek(a).empty();
        return jel ? std::string(a.arg) : "";
    }
};

struct f_fonda {
    radno_mesto& rame;
public:
    f_fonda(radno_mesto& rm) : rame(rm) {}
    std::string operator()(parfje& a) {
        auto jel = !a.tek(a).empty();
	if (jel) {
            const auto rzlt = magazinovicy::zovi(rame, a.arg, "");
            a.tek = f_bool(jel);
	    return rzlt;
	}
	else {
            return "";
	}
    }
};

struct f_inacxe {
    std::string operator()(parfje& a) {
        auto jel = !a.tek(a).empty();
        return jel ? "" : std::string(a.arg);
    }
};

struct f_konst {
    std::string s;
public:
    f_konst(std::string_view s_) : s(s_) {}
    std::string operator()(parfje&) {
        return s;
    }
};

struct f_spram_rez {
    const std::string tatko;
    bool uhvacyen;
public:
  f_spram_rez(std::string_view s, bool b) : tatko(s), uhvacyen(b) {}
    std::string operator()(parfje& a) {
        auto xt = tatko;
        // Sledecyi red mozxe da "ubije" upravo ovaj objekat i
        // onda cxitanye cxlanica objekta ne uspeva, pa mora
        // da se sacxuvaju oni koji nam trebaju.
	// Resxiti tako sxto 'a.tek' postane nesxto poput
	// std::shared_ptr<>.
        a.tek = f_konst(uhvacyen ? "⊤" : "");
        return xt;
    }
};

struct f_spram {
    std::string operator()(parfje& a) {
        a.tek = f_spram_rez(a.arg, false);
        return "";
    }
};

struct f_fspram {
    radno_mesto& rame;
public:
    f_fspram(radno_mesto& rm) : rame(rm) {}
    std::string operator()(parfje& a) {
        a.tek = f_spram_rez(magazinovicy::zovi(rame, a.arg, ""), false);
        return "";
    }
};

struct f_kad_rez {
    std::string tatko;
    bool rez;
    bool uhvacyen;
public:
    f_kad_rez(std::string_view s, bool b, bool u) : tatko(s), rez(b), uhvacyen(u) {}
    std::string operator()(parfje& a) {
	auto rzlt =  (a.arg.empty() ? uhvacyen : rez) ? "⊤" : "";
	a.tek = f_spram_rez(tatko, uhvacyen);
        return rzlt;
    }
};

struct f_kad {
    std::string operator()(parfje& a) {
        auto tatko = a.tek(a);
        auto jel = tatko == a.arg;
	auto uhvacyen = !a.tek(a).empty();
	a.tek = f_kad_rez(tatko, jel, uhvacyen || jel);
        return "";
    }
};


struct f_drukcxe {
    std::string operator()(parfje& a) {
        a.tek(a); // (treba da) menya a.tek
        auto jel = !a.tek(a).empty();
        return jel ? "" : std::string(a.arg);
    }
};

struct f_osiguraj {
    std::string operator()(parfje& a) {
        if (netacxno(a.tek(a))) {
            throw std::runtime_error("Neuspelo 'osiguraj'");
	}
        return "";
    }
};




radno_mesto::radno_mesto()
{
    biblioteka["iota"] = f_iota{};
    biblioteka["jedini"] = f_jedini{};
    biblioteka["negiraj"] = f_negiraj{};

    biblioteka["regex"] = f_regex();

    biblioteka["uvedi"] = f_uvedi(*this);
    biblioteka["uvedi_novu"] = f_uvedi_novu(*this);
    biblioteka["izvrsxi"] = f_izvrsxi();
    biblioteka["odbaci"] = f_odbaci();
    biblioteka["zavrsxi"] = f_zavrsxi();
    biblioteka["zovi"] = f_zovi(*this);

    biblioteka["fako"] = f_fako(*this);
    biblioteka["ako"] = f_ako();
    biblioteka["onda"] = f_onda();
    biblioteka["fonda"] = f_fonda(*this);
    biblioteka["inacxe"] = f_inacxe();
    biblioteka["spram"] = f_spram();
    biblioteka["fspram"] = f_fspram(*this);
    biblioteka["kad"] = f_kad();
    biblioteka["drukcxe"] = f_drukcxe();
    biblioteka["osiguraj"] = f_osiguraj();

    tekucyi.tek = f_nisx();
}

#include <iostream>
std::string radno_mesto::zovi(std::string_view fju, std::string_view arg)
{
    tekucyi.arg = arg;
    return biblioteka.at(std::string(fju))(tekucyi);
}


bool radno_mesto::ima(std::string_view fja) const
{
    return biblioteka.count(std::string(fja)) == 1;
}


bool radno_mesto::mozxe(std::string_view fja) const
{
    return (fja.find('_') == 0) || ima(fja);
}
