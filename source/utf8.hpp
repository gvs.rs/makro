/* -*- c-file-style:"stroustrup"; indent-tabs-mode: nil -*- */
/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2024-2024
 * @license GPLv3
 ***********************************************/

#if !defined(INC_UTF8)
#define INC_UTF8

#include <cstdint>

/** UTF-8:

    range	   Byte 1   Byte 2    Byte 3  Byte 4
    --------------------------------------------------
    0000 - 007F	   0xxxxxxx
    0080 - 07FF	   110xxxxx 10xxxxxx
    0800 - FFFF	   1110xxxx 10xxxxxx 10xxxxxx
    10000 - 10FFFF 11110xxx 10xxxxxx 10xxxxxx 10xxxxxx
*/


constexpr uint8_t utf8_prvi_od_dva_maska = 0xE0;
constexpr uint8_t utf8_prvi_od_dva = 0xC0;
constexpr uint8_t utf8_drugi = 0x80;
constexpr uint8_t utf8_sadrzxaj_drugih = 0x3F;

/** Prvi Junikod cyirilicxni znak u "glavnom skupu", gde su vecyina
    zxivih cyirilcxnih znakova */
constexpr char32_t prvi_cyiril = 0x400;

/** Poslednyi Junikod cyirilicxni znak u "glavnom skupu", gde su vecyina
    zxivih cyirilcxnih znakova */
constexpr char32_t poslednyi_cyiril = 0x45F;

/** Prvi Junikod znak iz Latin-Extended-A, gde su i srpska
    latinicxna slova (rasxtrkana od 0x108 do 0x17E). */
constexpr char32_t prvi_latin_extended_a = 0x100;

/** Poslednyi Junikod znak iz Latin-Extended-A, gde su i srpska
    latinicxna slova (rasxtrkana od 0x108 do 0x17E). */
constexpr char32_t poslednyi_latin_extended_a = 0x100;


/** Prvi Junikod znak iz Latin-1 Supplement, sxto je slicxno
    kao ISO 8859-1, a ima i dva matematicxka znaka. */
constexpr char32_t prvi_latin1_supplement = 0xC0;

/** Poslednyi Junikod znak iz Latin-1 Supplement, sxto je slicxno
    kao ISO 8859-1, a ima i dva matematicxka znaka. */
constexpr char32_t poslednyi_latin1_supplement = 0xFF;

constexpr bool utf8_znak_prvi_od_dva(int c) {
    return (c & utf8_prvi_od_dva_maska) == utf8_prvi_od_dva;
}

constexpr bool utf8_znak_drugi(int c) {
    return (c & ~utf8_sadrzxaj_drugih) == utf8_drugi;
}

constexpr char16_t utf8_od(int c, int c2) {
    return (c2 & utf8_sadrzxaj_drugih) | ((c & ~utf8_prvi_od_dva_maska) << 6);
}


#endif  // !defined(INC_UTF8)
