/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-20224
 * @license GPLv3
 ***********************************************/

#include "source/template.h"
#include "source/pscript.h"
#include "source/streams.h"


static void Potpis()
{
    printf("MACRO razvijac V1.6, (C) GVS, 2000-2024\n");
}


int main(int iArgc, char **psArgv)
{
    Potpis();
    if (iArgc != 4) {
        printf("Upotreba: makro <ime skripta> <ime sablona> <izlazni fajl>\n");
        return 1;
    }

    try {
        KDatTok script(psArgv[1], KTok::Rezxim::Cxitaj);
        if (!script.JeValidan()) {
            printf("Nije uspelo otvaranje skript-fajla\n");
            return 1;
        }

        KDatTok templ(psArgv[2], KTok::Rezxim::Cxitaj);
        if (!templ.JeValidan()) {
            printf("Nije uspelo otvaranje fajla sa sablonom\n");
            return 1;
        }

        KDatTok out(psArgv[3], KTok::Rezxim::Pisxi);
        if (!out.JeValidan()) {
            printf("Nije uspelo otvaranje izlaznog fajla\n");
            return 1;
        }

        try {
            PrevediTemplate(templ, &out, script);
        }
        catch (XTemplate& xTemplate) {
            printf("Greska u sablonu(%d): %s\n", templ.Red(), xTemplate.what());
            return 1;
        }
        catch (XSkript& xSkript) {
            printf("Greska u skriptu %s(%d): %s\n",
                xSkript.ImeDat().c_str(),
                xSkript.Red(),
                xSkript.UzmiErr().c_str()
            );
            return 1;
        }
    }
    catch (std::exception &ex) {
        printf("Nepoznat izuzetak: %s\n", ex.what());
        return 1;
    }

    printf("Prevodjenje uspesno izvrseno\n");

    return 0;
}

