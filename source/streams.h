/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#if !defined INC_STREAMS
#define INC_STREAMS

//#include <cstring>
#include <string>
#include <stdexcept>
#include <vector>


class KTok {
public:

    enum class Rezxim {
        Cxitaj,
        Pisxi
    };

    KTok(Rezxim eMod) : rezxim(eMod), red(1) {}

    virtual ~KTok() {}

    virtual int Pisxi(char c) = 0;
    virtual int Cxitaj();

    virtual int Pisxi(const char *sText);
    int Cxitaj(char *sText, int iLen);
    std::string CxitajDoKraja();
    virtual int Unread(int iChar);

    std::string CxitajRedDo(int ch) {
        std::string rzlt;
        for (int iChar = this->Cxitaj(); iChar != ch; iChar = this->Cxitaj()) {
            switch (iChar) {
            case EOF:
                throw std::invalid_argument("Neocekivani kraj datoteke");
            case '\n':
            case '\r':
                throw std::invalid_argument("Neocekivani kraj reda");
            default:
                rzlt += iChar;
            }
        }
        return rzlt;
    }

    std::string CxitajSve() {
        Iznova(Rezxim::Cxitaj);
        return CxitajDoKraja();
    }

    /** 'Vraca' dati niz znakova nazad u tok, znak po znak. Dati niz znakova se
        ne posmatra kao ASCIIZ string, dakle, moze da sadrzi i NUL znak. Dakle,
        uvek se vraca dati broj znakova.
        @param sText Niz znakova za vracanje
        @param iLen Broj znakova za vracanje
        @return 0 svi znakovi uspesno vraceni, inace nisu svi (mozda neki jesu)
        */
    int Unread(char *sText, size_t iLen);

    virtual int Iznova(Rezxim eMod) {
        rezxim = eMod;
        red = 1;
        return 0;
    }

    unsigned Red() const { return red; }

protected:
    Rezxim rezxim;
    std::vector<int> cxabar;
    unsigned red;
};


class KDatTok : public KTok {
    public:
    KDatTok(std::string ime, Rezxim eMod);
    ~KDatTok();

    int Pisxi(char c) override;
    int Pisxi(const char *sText) override {
        return KTok::Pisxi(sText);
    }
    int Cxitaj() override;
    int Iznova(Rezxim eMod) override;

    int JeValidan() const {
        return pFile != nullptr;
    }
    const char *ImeDat() const {
        return imedat.c_str();
    }

    protected:
    FILE *pFile;
    std::string imedat;
};


class KMemTok : public KTok {
    public:
    KMemTok(Rezxim eMod);
    ~KMemTok() {}

    int Pisxi(char c) override;
    int Cxitaj() override;
    int Iznova(Rezxim eMod) override;
    int Pisxi(const char *sText) override;

    protected:
    int poz;
    std::string str;
};

#endif // !defined INC_STREAMS
