/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2024-2024
 * @license GPLv3
 ***********************************************/

#if !defined(INC_BBJEZIK)
#define INC_BBJEZIK

/* Programski jezik BB.
 *  
 * Zamisxlyen u svrhe testiranya funkcija u MAKRO razvoju,
 * ovaj programski jezik ima i umereno uopsxtenu primenu.
 * Kad god name treba da na osnovu neki parametara dodyemo
 * do neke niske, gde su nam dovolyne osnovne mocucynosti
 * nadovezivanya i grananya.
 *
 * Sintaksa BB je namerno prosta i donekle podsecya na sintaksu
 * funkcija u MAKRO razovojima.
 *
 * Svaki izraz pocixnye simbolom, koji je isto definisan kao u MAKRO
 * razvojima. Kraj simbola se prepozna tacxkom (opet kao u
 * MAKRO). Simbol predstavlya ime funkcije (standardne ili
 * korisxnicxke) ili specijalni simbol BB (koji nije deo MAKRO razvoja).
 *
 * Nakon tacxke sledi niz znakova koji predstavlya parameter funkcije
 * i zavrsxava se sa tacxka-zarez. Ako se zxeli da u parametru postoji
 * tacxka zarez, pisxu se dva tacxka-zareza zaredom.
 *
 * Od specijalnih simbola, trenutno postoji samo `prm` sa znacxenyem
 * "parametar", s tim sxto parametar (u smislu goreopisani sintakse
 * BB) ovog specijalnog simola ima slozxeniju sintaksu, u smislu da se
 * ime parametra koji se uvodi, a koje mora da bude simbol, razdvojeno
 * znakom jednakosti od vrednosti parametra koji se uvodi.
 * 
 * Primer:
 *
 * prm.x=3;
 * ako.x;onda.Zec;
 *
 * Izvrsxavanye ovog programa vracya "Zec", jer `x` ima vrednost koja
 * se smatra "tacxnim".
 *
 * prm.x=3;
 * ako.x;inacxe.Zec;
 *
 * Posxto je uslov `inacxe` obrnut u odnosu na `onda`, rezultat jq
 * prazna niska "".
 *
 * regex/zec/magarac/;uvedi._zecmag;
 * _zecmag.zec;
 *
 * Rezultat je "magarac", a ovim se pokazuje da je definisanye korisnicxkih
 * funkcija isto kao i u MAKRO razvojima.
 */

#include <string>
#include <string_view>
#include <unordered_map>

#include "funkcije.hpp"

/** Izvrsxava dati @p program u BB jeziku i vracya rezultat koji je
    niska.  Iako u samom programu mogu da se definisxu parametri i
    koriste, ova funkcija i prima @p param koji mogu da se koriste u
    programu, nesxto poput okruzxenya (environment) u POSIX.
*/
std::string bbj_izvrsxi(std::string_view program, radno_mesto& rame, std::unordered_map<std::string, std::string> const& param = {});


#endif // !defined(INC_BBJEZIK)
