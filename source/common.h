/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/

#if !defined INC_COMMON
#define INC_COMMON


#include <string>


const int g_iMaxDuzinaIdent_C = 32;

typedef char Identifikator_T[g_iMaxDuzinaIdent_C + 1];

class KTok;

int UcitajIdentifikator(KTok& ulaz, Identifikator_T& sIdentifikator);

struct DubokIdentifikator {
    Identifikator_T makro;
    std::string param;
};

/** Ovo ucxitava dva identifikatora razdvojena tacxkom, poput
    `Makro.Param`.

    Ako ima samo jedan identifator, vracya kao Param, a Makro je
    prazan.  Ako se zavrsxava tacxkom i fali drugi identifikator, onda
    prvi vracya kao Makro, a Param je prazan.  Ako pocxinye tacxkom,
    recimo `.Param`, kao da nema nijedan identifikator.

    Naravno, ako nema nijedan identifikator, vracya oba prazna.
 */
DubokIdentifikator UcxitajDubokiIdentifikator(KTok& ulaz);

void PreskociDoKrajaReda(KTok& ulaz);

void PreskociBeline(KTok& ulaz);

constexpr int JeBelina(int iChar) {
    return (iChar == ' ' || iChar == '\t' || iChar == '\n' || iChar == '\r');
}


#endif // !defined INC_COMMON
