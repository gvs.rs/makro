/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2024
 * @license GPLv3
 ***********************************************/


#if !defined INC_TEMPLATE
#define INC_TEMPLATE

#include <stdexcept>
#include <string>


class XMakro;
class XTemplate : public std::runtime_error {
    public:
    XTemplate(const std::string &s) : std::runtime_error(s) {}
    static XTemplate od(XMakro const& ex);
};

class KDatTok;
void PrevediTemplate(KDatTok& ulaz, KDatTok *pIzlaz, KDatTok& skript);

#endif
