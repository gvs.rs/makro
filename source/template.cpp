/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2019
 * @license GPLv3
 ***********************************************/

#include "source/template.h"
#include "source/rmacro.h"
#include "source/pscript.h"
#include "source/streams.h"

#include <algorithm>
#include <string_view>


static std::string_view m_svKoristi = "KORISTI";
static std::string_view m_svRazvij = "RAZVIJ";

const int m_iMacroFileNameLen_C = 256;


static void UkljuciDatoteku(std::string const& imedat, KDatTok *pIzlaz)
{
    KDatTok ulaz(imedat.c_str(), KTok::Rezxim::Cxitaj);
    if (!ulaz.JeValidan()) {
        throw XTemplate("Nije uspelo otvaranje datoteke za ukljucivanje u sablon");
    }

    for (int cita = ulaz.Cxitaj(); cita != EOF; cita = ulaz.Cxitaj()) {
        pIzlaz->Pisxi(static_cast<char>(cita));
    }
}


void PrevediTemplate(KDatTok& ulaz, KDatTok *pIzlaz, KDatTok& skript)
{
    int iChar = ulaz.Cxitaj();
    if (iChar != '@')
        throw XTemplate("Prvi znak sxablon-datoteke mora da bude '@'");

    Identifikator_T tIdent = "";
    if (UcitajIdentifikator(ulaz, tIdent) || (m_svKoristi != tIdent))
        throw XTemplate("Sxablon-datoteka mora da pocxne sa '@KORISTI'");

    iChar = ulaz.Cxitaj();
    if (iChar == EOF)
        throw XTemplate("Neocekivan kraj ulaznog fajla");
    if (!JeBelina(iChar))
        throw XTemplate("Nepropisno zadato ime fajla sa makroima");

    PreskociBeline(ulaz);

    std::string imeMakroDat{ulaz.ImeDat()};
    try {
        if (auto pos = imeMakroDat.find_last_of("/\\"); pos != imeMakroDat.npos) {
            imeMakroDat.resize(pos+1);
        }
	else {
            imeMakroDat.clear();
	}
        imeMakroDat += ulaz.CxitajRedDo('\n');
        PreskociBeline(ulaz);
    }
    catch (std::exception& ex) {
        throw XTemplate(std::string("Prvi red sablona mora da pocne sa '@KORISTI <ime_makro_datoteke>': ") + ex.what());
    }

    KDatTok macrofile(imeMakroDat, KTok::Rezxim::Cxitaj);
    if (!macrofile.JeValidan())
        throw XTemplate(std::string("Neuspelo otvaranje makro-datoteke: ") + imeMakroDat);

    try {
        UcitajMakroe(macrofile);
    }
    catch (XMakro& ex) {
        throw XTemplate::od(ex);
    }

    for (iChar = ulaz.Cxitaj(); iChar != EOF; iChar = ulaz.Cxitaj()) {
        if (iChar != '@') {
            pIzlaz->Pisxi((char)iChar);
        }
        else switch (iChar = ulaz.Cxitaj()) {
        case '/':
            PreskociDoKrajaReda(ulaz);
            pIzlaz->Pisxi('\n');
            break;
        case '@':
            pIzlaz->Pisxi('@');
            break;;
        case '"':
            UkljuciDatoteku(ulaz.CxitajRedDo('"'), pIzlaz);
            break;
        default:
        {
            ulaz.Unread(iChar);

            Identifikator_T tRazvoj = "", tIdent = "";

            if (UcitajIdentifikator(ulaz, tIdent) || (m_svRazvij != tIdent))
                throw XTemplate("Nije prepoznata RAZVIJ sekcija");

            PreskociBeline(ulaz);
            if (UcitajIdentifikator(ulaz, tRazvoj))
                throw XTemplate("Lose ime razvoja u RAZVIJ sekciji");

            auto& razvoji = UzmiRazvoje();
            auto it = std::find_if(razvoji.begin(), razvoji.end(), [&](auto& raz) {
                return raz->ime == tRazvoj;
            });
            if (it == razvoji.end()) {
                throw XTemplate("Nije prepoznat razvoj");
            }

            iChar = ulaz.Cxitaj();
            if (iChar != EOF && !JeBelina(iChar)) {
                throw XTemplate("Nepropisan zavrsetak sekcije RAZVIJ");
            }
            if (skript.Iznova(KTok::Rezxim::Cxitaj)) {
                throw XTemplate("Nije uspelo ponovno otvaranje skript-datoteke");
            }

            ObradaSkripta(skript, pIzlaz, *it);
        }}
    }
}


XTemplate XTemplate::od(XMakro const& ex) {
    char s[256];
    snprintf(s, sizeof s, "makroteka(%d): %s", ex.Tok().Red(), ex.what());
    return {s};
}
