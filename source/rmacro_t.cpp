/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2018
 * @license GPLv3
 ***********************************************/

#include "source/rmacro.h"
#include "source/streams.h"

#include <iostream>
#include <unordered_map>

#include <cstdint>


struct ErrInfo {
    int red;
    std::string opis;
};

static std::unordered_map<char const*, ErrInfo> greska = 
    { { "test/rmacro/test1.mac",  { 1,  "Nedostaje sekcija deklaracija - nema klyucxne recxi na pocxetku" }}
    , { "test/rmacro/test2.mac",  { 1,  "Nedostaje sekcija deklaracija - ocxekivan DEKLARACIJE procxitano DEKLARUCIJE" }}
    , { "test/rmacro/test3.mac",  { 1,  "Nedostaje sekcija deklaracija - posle klyucxne recxi potreban je razmak" }}
    , { "test/rmacro/test4.mac",  { 3,  "U deklaraciji ponovljeno ime makroa 'Makro1'" }}
    , { "test/rmacro/test5.mac",  { 2,  "Ponovljeno ime parametra 'par1'" }}
    , { "test/rmacro/test6.mac",  { 6,  "Nepoznata kljucna rec 'KUKU'" }}
    , { "test/rmacro/test7.mac",  { 6,  "Ime razvoja nepravilno ''" }}
    , { "test/rmacro/test8.mac",  { 7,  "Prethodni razvoj 'PERA' nije kompletiran, fali 'Makro1'" }}
    , { "test/rmacro/test9.mac",  { 8,  "Prethodni razvoj 'PERA' nije kompletiran, fali 'Makro2'" }}
    , { "test/rmacro/test10.mac", { 8,  "Nepoznata kljucna rec 'MARKO'" }}
    , { "test/rmacro/test11.mac", { 8,  "Makro 'Makro3' nije deklarisan" }}
    , { "test/rmacro/test12.mac", { 8,  "Nedeklarisan parametar 'par4' makroa 'Makro2'" }}
    , { "test/rmacro/test13.mac", { 8,  "Previse parametara za makro 'Makro2'" }}
    , { "test/rmacro/test14.mac", { 8,  "Premalo parametara za makro 'Makro2', prvi koji fali je 'par3'" }}
    , { "test/rmacro/test15.mac", { 7,  "Ocekivan zarez u zaglavlju makroa 'Makro1'" }}
    , { "test/rmacro/test16.mac", { 7,  "Ocekivano ime parametra 'par1' u makrou 'Makro1'" }}
    , { "test/rmacro/test17.mac", { 9,  "Prethodni razvoj 'PERA' nije kompletiran, fali 'Makro2'" }}
    , { "test/rmacro/test18.mac", { 9,  "Prethodni razvoj 'PERA' nije kompletiran, fali 'Makro2'" }}
    , { "test/rmacro/test19.mac", { 7,  "Koriscen nepoznat parametar 'par3' u makrou 'Makro1'" }}
    , { "test/rmacro/test20.mac", { 7,  "Prazan parametar u telu makroa 'Makro1'" }}
    , { "test/rmacro/test21.mac", { 9,  "Neocekivani kraj datoteke u telu makroa 'Makro2'" }}
    , { "test/rmacro/test22.mac", { 9,  "Fali @ za kraj parametra u telu makroa 'Makro2'" }}
    , { "test/rmacro/test23.mac", { 6,  "Nepoznata kljucna rec 'PERIC'" }}
    , { "test/rmacro/test24.mac", { 6,  "Razvoj PERA Ocxekivan identifikator ili pocxetak komentara, dobijen znak: '!'" }}
    , { "test/rmacro/test25.mac", { 6,  "Ne moze razvoj da menja samog sebe!" }}
    , { "test/rmacro/test26.mac", { 6,  "Ime razvoja nepravilno ''" }}
    , { "test/rmacro/test27.mac", { 6,  "Greska u imenu izvornog razvoja za razvoj 'PERA'" }}
    , { "test/rmacro/test28.mac", { 6,  "Ne postoji originalni razvoj 'LAZA'" }}
    , { "test/rmacro/test29.mac", { 10, "Ponovljeno ime razvoja 'PERA'" }}
    , { "test/rmacro/test30.mac", { 12, "U definiciji ponovljeno ime makroa 'Makro1'" }}
    , { "test/rmacro/test31.mac", { 4,  " Posle prvog znaka komentara, ocxekivan drugi '/' a dobijen: '!'" }}
    , { "test/rmacro/test32.mac", { 4,  " Ocxekivan identifikator ili pocxetak komentara, dobijen znak: '#'" }}
};

static void PrintStr(const char *s)
{
    while (*s != '\0') {
        if ((uint8_t)*s < 32 || (uint8_t)*s >= 128 || *s == '\"')
            printf("\\x%02X", *s);
        else
            printf("%c", *s);
        ++s;
    }
}

inline void PrintStr(std::string const& s) {
    PrintStr(s.c_str());
}

static void Printout()
{
    printf("RAZVOJI:\n");
    for (auto razv : UzmiRazvoje()) {
        printf("    %s\n", razv->ime.c_str());
    }

    printf("MAKROI:\n");
    MakroPrintout();
}


int main(int argc, char **argv)
{
    if (argc != 2) {
        printf("Greska u pozivu testa: ocekivan 1 parametar\n");
        return 1;
    }
    std::string koren = argv[1];
    bool pao = false;

    for (auto gr : greska) {
        auto pStream = std::make_unique<KDatTok>((koren + "/" + gr.first).c_str(), KTok::Rezxim::Cxitaj);
        if (!pStream->JeValidan()) {
            printf("Greska u pozivu testa: ne moze da se otvori datoteka %s\n", gr.first);
            return 1;
        }

        bool blErr = false;
        try {
            UcitajMakroe(*pStream);
        }
        catch (XMakro& x) {
            if ((pStream->Red() != gr.second.red) ||
                (x.what() != gr.second.opis)) {
                pao = true;
                printf("  Ulazna datoteka '%s'\n", gr.first);
                printf("  Ocxekivan red %d, dobijeni red %d\n", gr.second.red, pStream->Red());
                printf("  Ocxekivana gresxka '%s'\n"
                       "    dobijena gresxka '%s'\n", gr.second.opis.c_str(), x.what());
            }
            blErr = true;
        }
	catch (std::exception& x) {
	    printf("  Ulazna datoteka '%s': Neocxekivan izuzetak %s\n", gr.first, x.what());
	}

        if (!blErr) {
            printf("  Ulazna datoteka '%s': Nije bilo gresaka u makro datoteci\n", gr.first);
            pao = true;
            Printout();
        }
    }

    return pao;
}

