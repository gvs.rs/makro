/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2003-2018
 * @license GPLv3
 ***********************************************/

#include "source/streams.h"

#include <cstdlib>

#include <memory>
#include <cstring>


#define CHECK(cond) if (cond) { } else Greska(#cond, __LINE__)

const char m_sFileName[] = "streams.tmp";


static void Greska(char const *sOpis, int iLine)
{
    printf("Neispunjen uslov: %s, red: %d\n", sOpis, iLine);
    exit(1);
}


static void TestFileStream(int blIznova = 0)
{
    printf("Test KDatTok ");
    if (blIznova)
        printf("(iznova)");
    else
        printf("(zatvori/otvori)");
    printf("...\n");

    auto pStream = std::make_unique<KDatTok>(m_sFileName, KTok::Rezxim::Pisxi);
    CHECK(pStream->JeValidan());

    KDatTok streamLos("", KTok::Rezxim::Pisxi);
    CHECK(!streamLos.JeValidan());
    CHECK(streamLos.Pisxi("abcde") != 0);
    CHECK(streamLos.Cxitaj() == EOF);

    CHECK(!strcmp(pStream->ImeDat(), m_sFileName));

    CHECK(pStream->Pisxi('a') == 0);
    CHECK(pStream->Red() == 1);
    CHECK(pStream->Pisxi('\n') == 0);
    CHECK(pStream->Red() == 2);
    CHECK(pStream->Pisxi("Text\n") == 0);
    CHECK(pStream->Red() == 3);
    CHECK(pStream->Cxitaj() == EOF);

    if (blIznova) {
        pStream->Iznova(KTok::Rezxim::Cxitaj);
    }
    else {
        pStream = std::make_unique<KDatTok>(m_sFileName, KTok::Rezxim::Cxitaj);
    }

    CHECK(pStream->Pisxi("abcde") != 0);

    CHECK(pStream->Red() == 1);
    CHECK(pStream->Cxitaj() == 'a');
    CHECK(pStream->Cxitaj() == '\n');
    CHECK(pStream->Red() == 2);
    CHECK(pStream->Unread('\n') == 0);
    CHECK(pStream->Red() == 1);
    CHECK(pStream->Cxitaj() == '\n');
    CHECK(pStream->Cxitaj() == 'T');
    CHECK(pStream->Cxitaj() == 'e');
    CHECK(pStream->Cxitaj() == 'x');
    CHECK(pStream->Cxitaj() == 't');
    CHECK(pStream->Cxitaj() == '\n');
    CHECK(pStream->Cxitaj() == EOF);

    if (blIznova) {
        pStream->Iznova(KTok::Rezxim::Cxitaj);
    }
    else {
        pStream = std::make_unique<KDatTok>(m_sFileName, KTok::Rezxim::Cxitaj);
    }

    char s[20+1];
    pStream->KTok::Cxitaj(s, 2);
    CHECK(!strcmp(s,"a\n"));
    pStream->KTok::Cxitaj(s, 20);
    CHECK(!strcmp(s,"Text\n"));
}


static void TestMemoryStream()
{
    printf("Test KMemDat ");
    printf("(iznova)\n");

    auto pStream = std::make_unique<KMemTok>(KTok::Rezxim::Pisxi);

    CHECK(pStream->Pisxi('a') == 0);
    CHECK(pStream->Pisxi('\n') == 0);
    CHECK(pStream->Pisxi("Text\n") == 0);
    CHECK(pStream->Cxitaj() == EOF);

    pStream->Iznova(KTok::Rezxim::Cxitaj);

    CHECK(pStream->Pisxi("abcde") != 0);

    CHECK(pStream->Cxitaj() == 'a');
    CHECK(pStream->Cxitaj() == '\n');
    CHECK(pStream->Unread('\n') == 0);
    CHECK(pStream->Cxitaj() == '\n');
    CHECK(pStream->Cxitaj() == 'T');
    CHECK(pStream->Cxitaj() == 'e');
    CHECK(pStream->Cxitaj() == 'x');
    CHECK(pStream->Cxitaj() == 't');
    CHECK(pStream->Cxitaj() == '\n');
    CHECK(pStream->Cxitaj() == EOF);

    pStream->Iznova(KTok::Rezxim::Cxitaj);

    char s[20+1];
    pStream->KTok::Cxitaj(s, 2);
    CHECK(!strcmp(s,"a\n"));
    pStream->KTok::Cxitaj(s, 20);
    CHECK(!strcmp(s,"Text\n"));
}


/*
 * Globalne funkcije
 */
int main()
{
    TestFileStream(0);
    TestFileStream(1);
    TestMemoryStream();

    printf("STREAMS test negativan\n");

	return 0;
}

