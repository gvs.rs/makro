/***********************************************
 * @program MAKRO razvijac
 * @copyright GVS Ko 2024-2024
 * @license GPLv3
 ***********************************************/

#include "bbjezik.hpp"

#include <iostream>

#include <cassert>


void prost()
{
    radno_mesto rame;
    auto        rzlt = bbj_izvrsxi("mile;;rile i gile vole disko", rame);
    assert(rzlt == "mile;rile i gile vole disko");
}


void prost_svoj_param()
{
    radno_mesto rame;
    auto        rzlt = bbj_izvrsxi("prm.x=3;"
                                   "ako.x;onda.Zec;",
                            rame);
    assert(rzlt == "Zec");
    rzlt = bbj_izvrsxi("prm.x=3;"
                       "ako.x;inacxe.Zec;",
                       rame);
    assert(rzlt.empty());
}


void prost_param()
{
    radno_mesto                                  rame;
    std::unordered_map<std::string, std::string> param = { { "x", "3" } };
    auto                                         rzlt = bbj_izvrsxi("ako.x;onda.Zec;", rame, param);
    assert(rzlt == "Zec");
    rzlt = bbj_izvrsxi("ako.x;inacxe.Zec;", rame, param);
    assert(rzlt.empty());
}


void regex()
{
    try {
        radno_mesto rame;
        auto        rzlt = bbj_izvrsxi("regex./zec/magarac/;uvedi._zecmag;"
                                       "_zecmag.zec;",
                                rame);
        assert(rzlt == "magarac");
    }
    catch (std::exception& ex) {
        std::cout << "Izuzetak! " << ex.what() << std::endl;
        throw;
    }
}


void iota()
{
    radno_mesto rame;
    auto        rzlt = bbj_izvrsxi("iota.;iota.;iota.;", rame);
    assert(rzlt == "012");
    rzlt = bbj_izvrsxi("iota.nov;uvedi._jota;iota.;iota.;_jota.", rame);
    assert(rzlt == "340");
}


void spram()
{
    radno_mesto                                  rame;
    std::unordered_map<std::string, std::string> param = { { "x", "3" } };

    constexpr std::string_view program =
        "spram.x;kad.2;onda.zec;kad.3;onda.magarac;kad.4;onda.vuk;drukcxe.tvor";

    auto rzlt = bbj_izvrsxi(program, rame, param);
    assert(rzlt == "magarac");

    param["x"] = "4";
    rzlt       = bbj_izvrsxi(program, rame, param);
    assert(rzlt == "vuk");

    param["x"] = "Zu";
    rzlt       = bbj_izvrsxi(program, rame, param);
    assert(rzlt == "tvor");

    constexpr std::string_view fprg =
        "spram.x;uvedi._x;fspram._x;kad.2;onda.zec;kad.3;onda.magarac;kad.4;onda.vuk;drukcxe.tvor";

    param["x"] = "3";
    rzlt = bbj_izvrsxi(fprg, rame, param);
    assert(rzlt == "magarac");

    param["x"] = "4";
    rzlt       = bbj_izvrsxi(fprg, rame, param);
    assert(rzlt == "vuk");

    param["x"] = "Zu";
    rzlt       = bbj_izvrsxi(fprg, rame, param);
    assert(rzlt == "tvor");
}


void izvrsxi()
{
    radno_mesto rame;

    bool gresxka = false;
    try {
        auto rzlt = bbj_izvrsxi("izvrsxi.", rame);
    }
    catch (std::invalid_argument&) {
        gresxka = true;
    }
    assert(gresxka);

    auto rzlt = bbj_izvrsxi("iota.nov;izvrsxi.", rame);
    assert(rzlt == "0");
}


void fakofonda()
{
    radno_mesto rame;

    auto rzlt = bbj_izvrsxi("spram.no;uvedi._zu;fako._zu;onda.a;inacxe.b", rame);
    assert(rzlt == "b");
    rzlt = bbj_izvrsxi("spram.da;uvedi._zu;fako._zu;onda.a;inacxe.b", rame);
    assert(rzlt == "a");
    rzlt = bbj_izvrsxi("spram.voli disko;uvedi._mile;spram.da;uvedi._zu;fako._zu;fonda._mile;inacxe.b", rame);
    assert(rzlt == "voli disko");
}


void osiguraj()
{
    radno_mesto rame;

    auto rzlt = bbj_izvrsxi("spram.T;osiguraj.", rame);
    assert(rzlt.empty());
    try {
        bbj_izvrsxi("spram.ne;osiguraj.", rame);
    }
    catch (std::runtime_error& ex) {
        assert(ex.what() == std::string_view("Neuspelo 'osiguraj'"));
    }
}


void negiraj()
{
    radno_mesto rame;

    auto rzlt = bbj_izvrsxi("spram.ne;negiraj.;osiguraj.;", rame);
    assert(rzlt == "");
}


void jedini()
{
    radno_mesto rame;

    auto rzlt = bbj_izvrsxi("jedini.a;jedini.b;osiguraj.;", rame);
    assert(rzlt == "");
    rzlt = bbj_izvrsxi("jedini.c;osiguraj.;", rame);
    assert(rzlt == "");
    bool izu;
#define TEST(x)	izu = false;						\
    try {								\
      rzlt = bbj_izvrsxi((x), rame);					\
    }									\
    catch (std::runtime_error& ex) {					\
      assert(ex.what() == std::string_view("Neuspelo 'osiguraj'"));	\
      izu = true;							\
    }									\
    assert(izu);
    TEST("jedini.a;osiguraj.;");
    TEST("jedini.b;osiguraj.;");
    TEST("jedini.c;osiguraj.;");
#undef TEST
}


int main()
{
    try {
        prost();
        prost_svoj_param();
        prost_param();
        regex();
        iota();
        spram();
        izvrsxi();
	fakofonda();
	osiguraj();
	negiraj();
	jedini();
    }
    catch (std::exception& ex) {
        std::cout << "Neocxekivan izuzetak " << ex.what() << std::endl;
        return -1;
    }

    return 0;
}
