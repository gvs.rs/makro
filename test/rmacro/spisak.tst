#
# Spisak testova za RMACRO
#
test\rmacro\test1.mac       # Nedostaje kljucna rec DEKLARACIJE
test\rmacro\test2.mac       # Losa kljucna rec DEKLARACIJE
test\rmacro\test3.mac       # DEKLARACIJE nije razdvojena od sledecih simbola
test\rmacro\test4.mac       # Dvaput deklarisan isti makro
test\rmacro\test5.mac       # Dva istoimena parametara u makrou
test\rmacro\test6.mac       # Nepoznata kljucna rec posle deklaracija
test\rmacro\test7.mac       # Lose ime razvoja
test\rmacro\test8.mac       # Prazan razvoj
test\rmacro\test9.mac       # Nepotpun razvoj
test\rmacro\test10.mac      # Nepoznata kljucna rec u razvoju
test\rmacro\test11.mac      # Nepoznat makro u razvoju
test\rmacro\test12.mac      # Nepoznat parametar u makrou u razvoju
test\rmacro\test13.mac      # Visak parametar u makrou u razvoju
test\rmacro\test14.mac      # Manjak parametar u makrou u razvoju
test\rmacro\test15.mac      # Fali zarez u makro zaglavlju u razvoju
test\rmacro\test16.mac      # Djubre u makro zaglavlju u razvoju
test\rmacro\test17.mac      # Jedan majmun mesto dva za kraj makroa
test\rmacro\test18.mac      # Nijedan majmun mesto dva za kraj makroa
test\rmacro\test19.mac      # Upotreba nepoznatog parametra u telu makroa
test\rmacro\test20.mac      # Upotreba loseg parametra u telu makroa
test\rmacro\test21.mac      # Neocekivan kraj datoteke telu makroa
test\rmacro\test22.mac      # Fali majmun za kraj parametra
test\rmacro\test23.mac      # Neka druga rec umesto "MENJA"
test\rmacro\test24.mac      # Neko djubre umesto "MENJA"
test\rmacro\test25.mac      # Razvoj hoce da menja samog sebe!
test\rmacro\test26.mac      # Djubre umesto imena razvoja
test\rmacro\test27.mac      # Djubre umesto imena izvornog razvoja
test\rmacro\test28.mac      # Nepoznat razvoj za menjanje
test\rmacro\test29.mac      # Ponovljeno ime razvoja
test\rmacro\test30.mac      # Dva puta menja isti makro

