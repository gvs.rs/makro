# The MAKRO processor

A generic macro processing tool with some similarity with C preprocessor 
[X-Macros](https://wikipedia/X_Macro). The focus is on generating many different text files from the
same set of definitions, like generating some structure/format in
several programming and human languages.  It is not tied  to C-like 
languages, but universally  applicable to any text file and has several 
features not available  in X-Macros.

Detailed documentation, in LaTeX, is available in the `doc` directory.

The functions subsystem is also, mostly for testing purposes, extracted
into a mini-programming language of its own, called BB (for historical
reasons). It is not explicitly documented, but it mostly resembles what you
get if you simply leave only the function calls from the macros.

# МАКРО препроцесор

Алат чија је основна функција да ради оно што [Џ-макрои](https://wikipedia/X_Macro)
изводе у Ц-препроцесору. Фокус је на прављењу великог броја текстуалних
датотека на основу једне заједничке дефиниције, попут генерисања
неког формата/структуре у више програмских и говорних језика. Али,
потпуно је независан од Ц-препроцесора и примењив на било који текст, 
са многим особинама које Џ-макрои немају.

Детаљна документација, у ЛаТеЏ формату, је у `doc` директоријуму.

Подсистем за функције је, пре свега у сврхе тестирања, издвојен у
засебан мини програмски језик, назван ББ из историјских разлога.
Није изричито документован, али углавном личи на оно што се добије
ако се оставе само позиви функција.
